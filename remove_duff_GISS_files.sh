#!/bin/bash

sudo synda remove cmip5.output1.NASA-GISS.GISS-E2-R.piControl.mon.seaIce.OImon.r1i1p1.v20121017.sic_OImon_GISS-E2-R_piControl_r1i1p1_333101-363012.nc
echo `ls /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/atmos/Amon/r1i1p1/v20121017/*_Amon_GISS-E2-R_piControl_r1i1p1_3[3456]*.nc` > files_to_remove
sed -i 's:/data/CMIP/cmip5:cmip5:g' files_to_remove
sed -i 's:/:.:g' files_to_remove
files=`cat files_to_remove`
for fil in $files
do 
   sudo synda remove -y $fil
done
rm $files
