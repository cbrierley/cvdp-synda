# Find the data for piControl
synda search model=CCSM4 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 piControl r1i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 piControl r1i1p1 OImon sic
sudo synda install -y model=CCSM4 piControl r1i1p1 OImon sic
synda search model=CCSM4 piControl r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 piControl r1i1p1 Omon msftmyz

# Find the data for midHolocene
synda search model=CCSM4 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 midHolocene r1i1p1 OImon sic
sudo synda install -y model=CCSM4 midHolocene r1i1p1 OImon sic
synda search model=CCSM4 midHolocene r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 midHolocene r1i1p1 Omon msftmyz
synda search model=CCSM4 midHolocene r2i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 midHolocene r2i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 midHolocene r2i1p1 OImon sic
sudo synda install -y model=CCSM4 midHolocene r2i1p1 OImon sic
synda search model=CCSM4 midHolocene r2i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 midHolocene r2i1p1 Omon msftmyz

# Find the data for lgm
synda search model=CCSM4 lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 lgm r1i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 lgm r1i1p1 OImon sic
sudo synda install -y model=CCSM4 lgm r1i1p1 OImon sic
synda search model=CCSM4 lgm r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 lgm r1i1p1 Omon msftmyz
synda search model=CCSM4 lgm r2i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 lgm r2i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 lgm r2i1p1 OImon sic
sudo synda install -y model=CCSM4 lgm r2i1p1 OImon sic
synda search model=CCSM4 lgm r2i1p1 Omon msftmyz
sudo synda install -y cmip5.output1.NCAR.CCSM4.lgm.mon.ocean.Omon.r2i1p1.v20140820 msftmyz 

# Find the data for historical
synda search model=CCSM4 historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.NCAR.CCSM4.historical.mon.atmos.Amon.r1i1p1.v20160829 variable=ts,tas,psl,pr
synda search model=CCSM4 historical r1i1p1 OImon sic
sudo synda install -y model=CCSM4 historical r1i1p1 OImon sic
synda search model=CCSM4 historical r1i1p1 Omon msftmyz
sudo synda install -y cmip5.output1.NCAR.CCSM4.historical.mon.ocean.Omon.r1i1p1.v20140820 msftmyz

# Find the data for 1pctCO2
synda search model=CCSM4 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 1pctCO2 r1i1p1 OImon sic
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 OImon sic
synda search model=CCSM4 1pctCO2 r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 Omon msftmyz

# Find the data for past1000
synda search model=CCSM4 past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 past1000 r1i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 past1000 r1i1p1 OImon sic
sudo synda install -y model=CCSM4 past1000 r1i1p1 OImon sic
synda search model=CCSM4 past1000 r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 past1000 r1i1p1 Omon msftmyz

# Find the data for rcp8.5
synda search model=CCSM4 rcp85 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.NCAR.CCSM4.rcp85.mon.atmos.Amon.r1i1p1.v20160829 variable=ts,tas,psl,pr
synda search model=CCSM4 rcp85 r1i1p1 OImon sic
sudo synda install -y cmip5.output1.NCAR.CCSM4.rcp85.mon.seaIce.OImon.r1i1p1.v20130228 sic
synda search model=CCSM4 rcp85 r1i1p1 Omon msftmyz
sudo synda install -y cmip5.output1.NCAR.CCSM4.rcp85.mon.ocean.Omon.r1i1p1.v20140820 msftmyz

# Find the data for abrupt4xCO2
synda search model=CCSM4 abrupt4xCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 abrupt4xCO2 r1i1p1 Amon variable=ts,tas,psl,pr
synda search model=CCSM4 abrupt4xCO2 r1i1p1 OImon sic
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 OImon sic
synda search model=CCSM4 abrupt4xCO2 r1i1p1 Omon msftmyz
sudo synda install -y cmip5.output1.NCAR.CCSM4.abrupt4xCO2.mon.ocean.Omon.r1i1p1.v20140820 msftmyz



# Make the $OUTDIR/namelist...
NAME="CCSM4"
OUTDIR=$NAME.output
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $NAME.driver.ncl
echo "CCSM4 0 piControl | /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20121128,v20130510,v20130514}/ | 250 | 1300" > $OUTDIR/namelist 
echo "CCSM4 0 midHolocene | /data/CMIP/cmip5/output1/NCAR/CCSM4/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120604,v20120524,v20121128}/ | 1000 | 1300" >> $OUTDIR/namelist
echo "CCSM4 1 midHolocene | /data/CMIP/cmip5/output1/NCAR/CCSM4/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r2i1p1/{v20120604,v20120529,v20121128}/ | 1269 | 1300" >> $OUTDIR/namelist
echo "CCSM4 0 lgm | /data/CMIP/cmip5/output1/NCAR/CCSM4/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120203,v20120524,v20121128}/ | 1800 | 1900" >> $OUTDIR/namelist
echo "CCSM4 1 lgm | /data/CMIP/cmip5/output1/NCAR/CCSM4/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r2i1p1/{v20120604,v20120524,v20140820}/ | 1870 | 1900" >> $OUTDIR/namelist
echo "CCSM4 0 historical | /data/CMIP/cmip5/output1/NCAR/CCSM4/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20160829,v20120202,v20121128,v20140820}/{ts,tas,psl,pr,msftmyz,}/ | 1850 | 2005" >> $OUTDIR/namelist
echo "CCSM4 0 1pctCO2 | /data/CMIP/cmip5/output1/NCAR/CCSM4/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20121031,v20120529,v20140820}/ | 1850 | 1989" >> $OUTDIR/namelist
echo "CCSM4 0 past1000 | /data/CMIP/cmip5/output1/NCAR/CCSM4/past1000/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120604,v20120411,v20121128}/ | 850 | 1850" >> $OUTDIR/namelist
echo "CCSM4 0 rcp85 | /data/CMIP/cmip5/output1/NCAR/CCSM4/rcp85/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20130228,v20160829,v20140820}/{ts,tas,psl,pr,msftmyz,}/ | 2006 | 2100" >> 
echo "CCSM4 0 abrupt4xCO2 | /data/CMIP/cmip5/output1/NCAR/CCSM4/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120604,v20140820}/ | 1850 | 2000" >> $OUTDIR/namelist

ncl -n $NAME.driver.ncl >& $NAME.driver.log &
