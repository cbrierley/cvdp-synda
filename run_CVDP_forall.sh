#!/bin/bash

#This script runs the CVDP for each fo the experiments after first building the namelists
#Presume GCMs have been run first

run_all_gcms="True"
run_all_expts="True"
run_vs_obs="True"

expts="piControl midHolocene lgm 1pctCO2 past1000" #note historical simulation analysed using run_vs_obs
gcms="bcc-csm1-1 CCSM4 CNRM-CM5 COSMOS-ASO CSIRO-Mk3-6-0 CSIRO-Mk3L-1-2 EC-EARTH-2-2 FGOALS-g2 FGOALS-gl FGOALS-s2 GISS-E2-R HadCM3 HadGEM2-CC HadGEM2-ES IPSL-CM5A-LR KCM1-2-2 MIROC-ESM MPI-ESM-P MRI-CGCM3"

# All of the single models
if [ $run_all_gcms == "True" ]
then
    for gcm in $gcms
    do
	OUTDIR=$gcm.output
	mkdir -p $OUTDIR
	sed "s:output/:$OUTDIR/:g" driver.ncl > $gcm.driver.ncl
	sed -i "s:Title goes here:$expt:g" $gcm.driver.ncl
	rm -f $OUTDIR/namelist
	for expt in $expts
	do 
	    grep $expt/ $gcm.commands.txt | while read line
	    do
		nml_ln=`echo $line | cut -d'"' -f2`
		echo $nml_ln >> $OUTDIR/namelist
	    done
	done
    done
    for gcm in $gcms
    do
	rm $gcm.driver.log
	ncl -n $gcm.driver.ncl >& $gcm.driver.log &
    done
fi

# All of the experiments
if [ $run_all_expts == "True" ]
then
    for expt in $expts
    do
      OUTDIR=output.$expt
      mkdir -p $OUTDIR
      sed "s:output/:$OUTDIR/:g" driver.ncl > $expt.driver.ncl
      sed -i "s:Title goes here:$expt:g" $expt.driver.ncl
      if [ $expt == "1pctCO2" ]
      then
         sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $expt.driver.ncl
         sed -i "s:climo_syear = 1971:climo_syear = -39:g" $expt.driver.ncl
         sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $expt.driver.ncl
      fi  
      rm -f $OUTDIR/namelist
      for gcm in $gcms
      do 
         grep $expt/ $gcm.commands.txt | while read line
	 do
           nml_ln=`echo $line | cut -d'"' -f2`
	   echo $nml_ln >> $OUTDIR/namelist
	 done
       done  
    done
    for expt in $expts
    do
       rm $expt.driver.log
       ncl -n $expt.driver.ncl >& $expt.driver.log &
    done
fi

if [ $run_vs_obs == "True" ]
then
  OUTDIR=output.vsObs
  mkdir -p $OUTDIR
  sed "s:output/:$OUTDIR/:g" driver.ncl > vsObs.driver.ncl
  sed -i "s:Title goes here:Historical vs Observations:g" vsObs.driver.ncl
  sed -i 's:obs = "False":obs = "True":g' vsObs.driver.ncl
  sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' vsObs.driver.ncl
  cp namelist_obs $OUTDIR/
  rm -f $OUTDIR/namelist
  echo "C20 Reanalysis | /home/ucfaccb/DATA/obs/C20_Reanal/ | 1871 | 2012" >> $OUTDIR/namelist
  for gcm in $gcms
  do 
    grep historical/ $gcm.commands.txt | while read line
    do
      nml_ln=`echo $line | cut -d'"' -f2`
      echo $nml_ln >> $OUTDIR/namelist
    done
  done
  rm vsObs.driver.log
  ncl -n vsObs.driver.ncl >& vsObs.driver.log &
fi
