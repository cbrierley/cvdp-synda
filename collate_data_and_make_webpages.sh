#!/bin/bash
#This script will curate all the data for access via the webpage.
#Initially it will build a directory and link to all the files. A known structure will further code distribution.
#Presumes all experiments have been run first.

DATA_DIR=data
data_expts="piControl vsObs midHolocene lgm 1pctCO2 past1000"
expts="piControl historical midHolocene lgm 1pctCO2 past1000"
gcms="bcc-csm1-1 CCSM4 CNRM-CM5 COSMOS-ASO CSIRO-Mk3-6-0 CSIRO-Mk3L-1-2 EC-EARTH-2-2 FGOALS-g2 FGOALS-gl FGOALS-s2 GISS-E2-R HadCM3 HadGEM2-CC HadGEM2-ES IPSL-CM5A-LR KCM1-2-2 MIROC-ESM MPI-ESM-P MRI-CGCM3"
ncl_scripts="cvdp_data.functions ENSOvsAnnCycle_stddev Abrametal2014.vs.SAM_ANN Bathianyetal2018_Fig1 DArrigoetal2003.vs.AO_JJA DiffEnsMean_nao_pr_regression_djf EmileGeayetal2013.vs.nino34_ann EmileGeayetal2016.vs.midHolocene_nino34 ENSOvsAnnCycle_stddev example_cvdp_data.functions_BrierleyWainer HadGEM2-ES_gm_tas_10yr_stddev plot_10stddev_midHolocene plotCVDPComparisonMaps_example plotDiffEnsMnMaps_example plot_SAm_TraCE_precip scatter_SAfr_Hopletetal"

mkdir -p $DATA_DIR
shopt -s extglob #set extra globbing on in script

##Create directory style webpages
#start with gcms
sed "s:sed_string:Climate Model:g" webpage_top.html > gcms.html
for gcm in $gcms
do
  ln -s $PWD/$gcm.output/ ~/public_html/PMIPVarData/$gcm
  echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/$gcm class=contenttype-folder state-published url><h4>$gcm</h4></a>" >> gcms.html
done
cat webpage_bottom.html >> gcms.html

#Move onto expts webpage
sed "s:sed_string:Experiment:g" webpage_top.html > expts.html
for expt in $expts
do
  ln -s $PWD/output.expt ~/public_html/PMIPVarData/output.$expt
  echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/output.$expt class=contenttype-folder state-published url><h4>$expt</h4></a>" >> expts.html
done
cat webpage_bottom.html >> expts.html

#Move onto data webpage. Populate both that and the output directory simultaneously
sed "s:sed_string:Output Data and Post-processing Scripts:g" webpage_top.html > data.html
echo "Personally I expect people to download one of the combined files below. These contain all the data for PMIP3 and some scripts to plot them. They are about 2Gb and there is a Mac/Linux one (tar.gz) and a Windows one (zip). Alternatively you can download any of the simulation output files or scripts below. All of the data files are on each model's native atmosphere grid. The attached scripts regrid everything onto a 1x1 grid before computation." >> data.html
echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/data/all_PMIP_cvdp_data.tar.gz class=contenttype-folder state-published url><h3>All Output Data (in tar.gz format)<h3></a>" >> data.html
echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/data/all_PMIP_cvdp_data.zip class=contenttype-folder state-published url><h3>All Output Data (in zip format)<h3></a>" >> data.html
#copy over some analysis codes
echo "<br>" >> data.html
echo "<h3>NCL Post-Processing Scripts<h3>" >> data.html
echo "These scripts are written in the language of NCL. This is a niche language that has been developed particularly for climate science purposes. Instructions on how to download it are <a href=https://www.ncl.ucar.edu/Download/>here</a>. You can edit the scripts themselves in any simple text editor (e.g. Textedit on Mac or WordPad on Windows) and run from the terminal by typing <tt>ncl ncl_scripts/scriptname.ncl</tt> from the directory that you've downloaded." >> data.html
for ncl_script in $ncl_scripts
do
  cp ncl_scripts/$ncl_script.ncl $DATA_DIR/
  echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/data/$ncl_script.ncl class=contenttype-folder state-published url><h4>$ncl_script</h4></a>" >> data.html
  if [ -e $ncl_script.png ]
  then
    convert -trim $ncl_script.png ~/public_html/PMIPVarData/images/$ncl_script.png
  fi	
done
echo "<br>" >> data.html
echo "<h3>Individual Simulation Output<h3>" >> data.html

# find the data.nc for all of the experiments
for expt in $data_expts
 do
   #only select main files (not .tas.trends_timeseries ones)
   ncfiles=`ls output.$expt/*.cvdp_data.+([0-9])-+([0-9]).nc`
	 for filename_wpath in $ncfiles
	 do
	   filename_nopath=`basename $filename_wpath`
     #echo copying $filename_nopath
	   cp $filename_wpath $DATA_DIR/$filename_nopath  
     echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/data/$filename_nopath class=contenttype-folder state-published url><h4>$filename_nopath</h4></a>" >> data.html
   done  
done

#create single tar.gz  file
tar -czf $DATA_DIR/all_PMIP_cvdp_data.tar.gz $DATA_DIR/*.nc $DATA_DIR/*.ncl
zip $DATA_DIR/all_PMIP_cvdp_data.zip $DATA_DIR/*.nc $DATA_DIR/*.ncl

#Finish off data webpage
cat webpage_bottom.html >> data.html
