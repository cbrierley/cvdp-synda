; Calculates a variety of oceanic indices, as well as hovmollers, spectra, 
; monthly standard deviations, running standard deviations, and spatial
; composites based on the nino3.4 index. 
;
; Variables used: pr
; NOTE TO CMB: THIS SCRIPT TAKES A RELATVIELY LONG TIME COMPARED TO SST.INDICES.  ARE THERE WAYS TO OPTIMISE IT BETTER?
;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$CVDP_SCRIPTS/functions.ncl"

begin
  print("Starting: pr.indices.ncl")
  
  OUTDIR      = getenv("OUTDIR") 
  SCALE_TIMESERIES = getenv("SCALE_TIMESERIES")  
  OUTPUT_DATA      = getenv("OUTPUT_DATA")  
  PNG_SCALE        = tofloat(getenv("PNG_SCALE"))
  OPT_CLIMO        = getenv("OPT_CLIMO")
  CLIMO_SYEAR      = toint(getenv("CLIMO_SYEAR"))
  CLIMO_EYEAR      = toint(getenv("CLIMO_EYEAR"))
  OUTPUT_TYPE      = getenv("OUTPUT_TYPE") 
  COLORMAP         = getenv("COLORMAP")  
  
  nsim = numAsciiRow(OUTDIR+"namelist_byvar/namelist_prect")
  na = asciiread(OUTDIR+"namelist_byvar/namelist_prect",(/nsim/),"string")
  names = new(nsim,"string")
  paths = new(nsim,"string")
  syear = new(nsim,"integer",-999)
  eyear = new(nsim,"integer",-999)
  delim = "|"

  do gg = 0,nsim-1
    names(gg) = str_strip(str_get_field(na(gg),1,delim))
    paths(gg) = str_strip(str_get_field(na(gg),2,delim))
    syear(gg) = stringtointeger(str_strip(str_get_field(na(gg),3,delim)))
    eyear(gg) = stringtointeger(str_strip(str_get_field(na(gg),4,delim)))
  end do
  delete(na)
  nyr = eyear-syear+1
  nyr_max = max(nyr)
;-------------------------------------------------------------------------------------------------
  
  wks_type = OUTPUT_TYPE
  if (wks_type.eq."png") then
    wks_type@wkWidth = 1500*PNG_SCALE
    wks_type@wkHeight = 1500*PNG_SCALE
  end if
  wks_neb = gsn_open_wks(wks_type,getenv("OUTDIR")+"nebrazil.timeseries")  
  wks_allindia = gsn_open_wks(wks_type,getenv("OUTDIR")+"allindia.timeseries")
  wks_eafrican = gsn_open_wks(wks_type,getenv("OUTDIR")+"eafrican.timeseries")
  wks_easian = gsn_open_wks(wks_type,getenv("OUTDIR")+"easian.timeseries")
  wks_hadewp = gsn_open_wks(wks_type,getenv("OUTDIR")+"hadewp.timeseries")
  wks_sahel = gsn_open_wks(wks_type,getenv("OUTDIR")+"sahel.timeseries")
  wks_baikal = gsn_open_wks(wks_type,getenv("OUTDIR")+"baikal.timeseries")
  wks_iberia = gsn_open_wks(wks_type,getenv("OUTDIR")+"iberia.timeseries")
  
  if (COLORMAP.eq.0) then
    gsn_define_colormap(wks_neb,"ncl_default")    
    gsn_define_colormap(wks_allindia,"ncl_default")  
    gsn_define_colormap(wks_eafrican,"ncl_default")  
    gsn_define_colormap(wks_easian,"ncl_default")   
    gsn_define_colormap(wks_hadewp,"ncl_default")
    gsn_define_colormap(wks_sahel,"ncl_default")
    gsn_define_colormap(wks_baikal,"ncl_default")
    gsn_define_colormap(wks_iberia,"ncl_default")
  end if
  if (COLORMAP.eq.1) then
    gsn_define_colormap(wks_neb,"ncl_default")    
    gsn_define_colormap(wks_allindia,"ncl_default")  
    gsn_define_colormap(wks_eafrican,"ncl_default")  
    gsn_define_colormap(wks_easian,"ncl_default")   
    gsn_define_colormap(wks_hadewp,"ncl_default")
    gsn_define_colormap(wks_sahel,"ncl_default")
    gsn_define_colormap(wks_baikal,"ncl_default")
    gsn_define_colormap(wks_iberia,"ncl_default")
  end if

  xyneb = new(nsim,"graphic")  
  xyallindia  = new(nsim,"graphic")  
  xyeafrican  = new(nsim,"graphic")  
  xyeasian = new(nsim,"graphic")  
  xyhadewp = new(nsim,"graphic")  
  xysahel = new(nsim,"graphic") 
  xybaikal = new(nsim,"graphic") 
  xyiberia = new(nsim,"graphic") 


  wgt = (/1.,2.,1./)   
  wgt = wgt/sum(wgt)
  pi=4.*atan(1.0)
  rad=(pi/180.)

  do ee = 0,nsim-1
    ppt = data_read_in(paths(ee),"PRECT",syear(ee),eyear(ee)) 
    if (isatt(ppt,"is_all_missing")) then
      delete(ppt)
      continue
    end if 

    ; Precipitation indices in observations are only defined over land,
    ;   so mask our oceanmask out land
    d = addfile("$NCARG_ROOT/lib/ncarg/data/cdf/landsea.nc","r")
    basemap = d->LSMASK
    lsm = landsea_mask(basemap,ppt&lat,ppt&lon)
    ppt = mask(ppt,conform(ppt,lsm,(/1,2/)).ge.1,True)
    delete([/lsm,basemap/])
    delete(d)  

    if (OPT_CLIMO.eq."Full") then
      climo = clmMonTLL(ppt)                 
      ppt = rmMonAnnCycTLL(ppt)
    else
      if CLIMO_SYEAR.lt.0.and.CLIMO_EYEAR.le.0 then
        ;if negative, the climatology is set relative to the run end.
        climo_syear=eyear(ee)+CLIMO_SYEAR
        climo_eyear=eyear(ee)+CLIMO_EYEAR
      else
        climo_syear=CLIMO_SYEAR
        climo_eyear=CLIMO_EYEAR
      end if                   
      check_custom_climo(names(ee),syear(ee),eyear(ee),climo_syear,climo_eyear)
      temp_arr = ppt
      delete(temp_arr&time)
      temp_arr&time = cd_calendar(ppt&time,-1)
      climo = clmMonTLL(temp_arr({climo_syear*100+1:climo_eyear*100+12},:,:))                 
      delete(temp_arr)
      ppt   = calcMonAnomTLL(ppt,climo) 
    end if

    coswgt=cos(rad*ppt&lat)
    coswgt!0 = "lat"
    coswgt&lat= ppt&lat

    
    ; North East Brazil (region taken from Roucou et al (1996, Int. J. Clim.)
    llats = -12.     
    llatn = 2.
    llonw = 360.-43.
    llone = 360.-36.
    nebrazil = wgt_areaave_Wrap(ppt(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    nebrazil@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    nebrazil@units = ppt@units
    nebrazil@long_name = "North-East Brazil rainfall (monthly)"       
    nebrazil_climo = wgt_areaave_Wrap(climo(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    nebrazil@AnnCycle = (/nebrazil_climo/)
    delete(nebrazil_climo)
    
    ;East African Monthly Rainfall [Taking IPCC AR5 Annex Definition
    llats = -11.3    ; eafrican
    llatn = 15.
    llonw = 25.
    llone = 52.
    eafrican = wgt_areaave(ppt(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    eafrican@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,eafrican)
    eafrican@units = ppt@units
    eafrican@long_name = "East African Rainfall (monthly)"       
    eafrican_climo = wgt_areaave_Wrap(climo(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    eafrican@AnnCycle = (/eafrican_climo/)
    delete(eafrican_climo)
    
    ;All India Rainfall - couldn't immediately see a commonly used box.
    ; Combining IPCC AR5 with India's lat/lon to give (5-30oN,68-95oE)
    llats = 5.    ; allindia
    llatn = 30.
    llonw = 68.
    llone = 95.
    allindia = wgt_areaave(ppt(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    allindia@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,allindia)
    allindia@units = ppt@units
    allindia@long_name = "allindia timeseries (monthly)"       
    allindia_climo = wgt_areaave_Wrap(climo(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    allindia@AnnCycle = (/allindia_climo/)
    delete(allindia_climo)
    
    ;East Asian Monsoon [20-40oN, 100-135oE (after IPCC AR5 Fig. 14.3]
    llats = 20.    
    llatn = 40.
    llonw = 100.
    llone = 135.
    easian = wgt_areaave(ppt(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    easian@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,easian)
    easian@units = ppt@units
    easian@long_name = "East Asian Monsoon (monthly)"       
    easian_climo = wgt_areaave_Wrap(climo(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    easian@AnnCycle = (/easian_climo/)
    delete(easian_climo)

    ;England Wales Precipitation ]
    ppttmp = lonFlip(ppt)
    llats = 50.    
    llatn = 55.
    llonw = -5.
    llone = 2.
    hadewp = wgt_areaave(ppttmp(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    hadewp@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,hadewp)
    hadewp@units = ppt@units
    hadewp@long_name = "England Wales Precip. (monthly)"       
    climotmp = lonFlip(climo)
    hadewp_climo = wgt_areaave_Wrap(climotmp(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    hadewp@AnnCycle = (/hadewp_climo/)
    delete(hadewp_climo)

    ;Iberia
    llats = 37.
    llatn = 43.
    llonw = -10.
    llone = -1.
    iberia = wgt_areaave(ppttmp(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    iberia@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,iberia)
    iberia@units = ppt@units
    iberia@long_name = "Iberian Precip. (monthly)"       
    iberia_climo = wgt_areaave_Wrap(climotmp(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    iberia@AnnCycle = (/iberia_climo/)
    delete(iberia_climo)

    ;Sahel Precipitation
    llats = 10.    
    llatn = 20.
    llonw = -20.
    llone = 10.
    sahel = wgt_areaave(ppttmp(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    sahel@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,sahel)
    sahel@units = ppt@units
    sahel@long_name = "Sahel Precipitation timeseries (monthly)"       
    sahel_climo = wgt_areaave_Wrap(climotmp(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    sahel@AnnCycle = (/sahel_climo/)
    delete(sahel_climo)

    ;Baikal Catchment Precipitation
    llats = 45.    
    llatn = 57.
    llonw = 95.
    llone = 112.
    baikal = wgt_areaave(ppt(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    baikal@comment_cvdp = "land average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    copy_VarCoords(nebrazil,baikal)
    baikal@units = ppt@units
    baikal@long_name = "Baikal Precip. timeseries (monthly)"       
    baikal_climo = wgt_areaave_Wrap(climo(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    baikal@AnnCycle = (/baikal_climo/)
    delete(baikal_climo)
    
    delete([/ppt,ppttmp,climo,climotmp,coswgt/]);tidy up, for next time in loop
    

;---------------------------------------------------------------------------------------------     
    if (OUTPUT_DATA.eq."True") then
      modname = str_sub_str(names(ee)," ","_")
      bc = (/"/","'","(",")"/)
      do gg = 0,dimsizes(bc)-1
        modname = str_sub_str(modname,bc(gg),"_")
      end do
      fn = getenv("OUTDIR")+modname+".cvdp_data.pr.indices."+syear(ee)+"-"+eyear(ee)+".nc"
      if (.not.isfilepresent2(fn)) then
        z = addfile(fn,"c")
        z@source = "NCAR Climate Analysis Section's Climate Variability Diagnostics Package v"+getenv("VERSION")
        z@notes = "Data from "+names(ee)+" from "+syear(ee)+"-"+eyear(ee)
        if (OPT_CLIMO.eq."Full") then
          z@climatology = syear(ee)+"-"+eyear(ee)+" climatology removed prior to all calculations (other than means)"
        else
          z@climatology = climo_syear+"-"+climo_eyear+" climatology removed prior to all calculations (other than means)"
        end if
        z@Conventions = "CF-1.6"
        date = cd_calendar(nebrazil&time,-1)
        date@long_name = "current date (YYYYMM)"
        delete(date@calendar)
        date!0 = "time"
        date&time = nebrazil&time
        date@units = "1"
        z->date = date
        delete(date)
      else
        z = addfile(fn,"w")
      end if
      z->nebrazil = set_varAtts(nebrazil,"","","")
      z->easian = set_varAtts(easian,"","","")
      z->eafrican  = set_varAtts(eafrican,"","","")
      z->allindia  = set_varAtts(allindia,"","","")
      z->hadewp = set_varAtts(hadewp,"","","")
      z->sahel = set_varAtts(sahel,"","","")
      z->baikal = set_varAtts(baikal,"","","")
      z->iberia = set_varAtts(iberia,"","","")
      delete([/modname,fn/])
    end if
    
;==========================================================================================
    xyres = True
    xyres@gsnDraw = False
    xyres@gsnFrame = False
    xyres@gsnRightString = ""
    xyres@gsnLeftString = ""
    xyres@gsnYRefLine = 0.0
    xyres@gsnYRefLineColor = "gray42"
    xyres@xyLineColor = "gray62"
    if (wks_type.eq."png") then
      xyres@xyLineThicknessF = .75  
    else
      xyres@xyLineThicknessF = .5  
    end if   
    xyres@tiYAxisString = ""
    if (nsim.le.5) then
      xyres@tmXBLabelFontHeightF = 0.0125
      xyres@tmYLLabelFontHeightF = 0.0125
      xyres@gsnLeftStringFontHeightF = 0.017
      xyres@gsnCenterStringFontHeightF = 0.017
      xyres@gsnRightStringFontHeightF = 0.013         
    else
      xyres@tmXBLabelFontHeightF = 0.018
      xyres@tmYLLabelFontHeightF = 0.018
      xyres@gsnLeftStringFontHeightF = 0.024
      xyres@gsnCenterStringFontHeightF = 0.024
      xyres@gsnRightStringFontHeightF = 0.020    
    end if
    xyres@vpXF = 0.05
    xyres@vpHeightF = 0.3
    if (SCALE_TIMESERIES.eq."True") then
      xyres@vpWidthF = 0.9*((nyr(ee)*1.)/nyr_max)
    else
      xyres@vpWidthF = 0.9
    end if    
    xyres@gsnCenterString = ""
    
    xyres@trXMinF = syear(ee)-.5
    xyres@trXMaxF = eyear(ee)+1.5
    xyres@tiMainOn = False
    
    
    xyres@gsnXYAboveFillColors = "aquamarine4"
    xyres@gsnXYBelowFillColors = "peru"
    xyres@gsnLeftString = names(ee)
    arr = new((/2,dimsizes(nebrazil)/),typeof(nebrazil))
    
    tttt = dtrend_msg(ispan(0,dimsizes(nebrazil)-1,1),nebrazil,False,True)
    arr(0,:) = (/ nebrazil /)
    arr(1,:) = (/  (ispan(0,dimsizes(nebrazil)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(nebrazil),2,True)+nebrazil@units+" "+nyr(ee)+"yr~S~-1~N~"
    xyneb(ee) = gsn_csm_xy(wks_neb,fspan(syear(ee),eyear(ee)+.91667,dimsizes(nebrazil)),arr,xyres) 
    delete(tttt)
    
    tttt = dtrend_msg(ispan(0,dimsizes(eafrican)-1,1),eafrican,False,True)
    arr(0,:) = (/ eafrican /)
    arr(1,:) = (/  (ispan(0,dimsizes(eafrican)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(eafrican),2,True)+eafrican@units+" "+nyr(ee)+"yr~S~-1~N~"
    xyeafrican(ee) = gsn_csm_xy(wks_eafrican,fspan(syear(ee),eyear(ee)+.91667,dimsizes(eafrican)),arr,xyres) 
    delete(tttt)
    
    tttt = dtrend_msg(ispan(0,dimsizes(allindia)-1,1),allindia,False,True)
    arr(0,:) = (/ allindia /)
    arr(1,:) = (/  (ispan(0,dimsizes(allindia)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(allindia),2,True)+allindia@units+" "+nyr(ee)+"yr~S~-1~N~"
    xyallindia(ee) = gsn_csm_xy(wks_allindia,fspan(syear(ee),eyear(ee)+.91667,dimsizes(allindia)),arr,xyres)
    delete(tttt)
    
    tttt = dtrend_msg(ispan(0,dimsizes(easian)-1,1),easian,False,True)
    arr(0,:) = (/ easian /)
    arr(1,:) = (/  (ispan(0,dimsizes(easian)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(easian),2,True)+easian@units+" "+nyr(ee)+"yr~S~-1~N~"
    xyeasian(ee) = gsn_csm_xy(wks_easian,fspan(syear(ee),eyear(ee)+.91667,dimsizes(easian)),arr,xyres)
    delete(tttt)
    
    tttt = dtrend_msg(ispan(0,dimsizes(hadewp)-1,1),hadewp,False,True)
    arr(0,:) = (/ hadewp /)
    arr(1,:) = (/  (ispan(0,dimsizes(hadewp)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(hadewp),2,True)+hadewp@units+" "+nyr(ee)+"yr~S~-1~N~"
    xyhadewp(ee) = gsn_csm_xy(wks_hadewp,fspan(syear(ee),eyear(ee)+.91667,dimsizes(hadewp)),arr,xyres)
    delete(tttt)
    
    tttt = dtrend_msg(ispan(0,dimsizes(sahel)-1,1),sahel,False,True)
    arr(0,:) = (/ sahel /)
    arr(1,:) = (/  (ispan(0,dimsizes(sahel)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(sahel),2,True)+sahel@units+" "+nyr(ee)+"yr~S~-1~N~"
    xysahel(ee) = gsn_csm_xy(wks_sahel,fspan(syear(ee),eyear(ee)+.91667,dimsizes(sahel)),arr,xyres)
    delete(tttt)
    
    tttt = dtrend_msg(ispan(0,dimsizes(baikal)-1,1),baikal,False,True)
    arr(0,:) = (/ baikal /)
    arr(1,:) = (/  (ispan(0,dimsizes(baikal)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(baikal),2,True)+baikal@units+" "+nyr(ee)+"yr~S~-1~N~"
    xybaikal(ee) = gsn_csm_xy(wks_baikal,fspan(syear(ee),eyear(ee)+.91667,dimsizes(baikal)),arr,xyres)

    tttt = dtrend_msg(ispan(0,dimsizes(iberia)-1,1),iberia,False,True)
    arr(0,:) = (/ iberia /)
    arr(1,:) = (/  (ispan(0,dimsizes(iberia)-1,1)*tttt@slope)+tttt@y_intercept /)
    xyres@gsnRightString = decimalPlaces(tttt@slope*dimsizes(iberia),2,True)+iberia@units+" "+nyr(ee)+"yr~S~-1~N~"
    xyiberia(ee) = gsn_csm_xy(wks_iberia,fspan(syear(ee),eyear(ee)+.91667,dimsizes(iberia)),arr,xyres)

   delete([/arr,tttt,xyres/])
    
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
;- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -       
    title_neb = nebrazil@comment_cvdp
    title_allindia  = allindia@comment_cvdp
    title_eafrican  = eafrican@comment_cvdp
    title_easian = easian@comment_cvdp
    title_hadewp = hadewp@comment_cvdp
    title_sahel = sahel@comment_cvdp
    title_baikal = baikal@comment_cvdp
    title_iberia = iberia@comment_cvdp
    
    delete([/nebrazil,eafrican,allindia,easian,sahel,hadewp,baikal,iberia/])
  end do    
;-------------------------------------------------------------
  ncol = floattointeger(sqrt(nsim))
  nrow = (nsim/ncol)+mod(nsim,ncol) 
  
  panres = True
  panres@gsnMaximize = True
  panres@gsnPaperOrientation = "portrait"
  panres@gsnPanelYWhiteSpacePercent = 3.0
  if (nsim.le.10) then
    panres@txFontHeightF = 0.016
  else
    panres@txFontHeightF = 0.012
  end if

  if (SCALE_TIMESERIES.eq."True") then
    tt = ind(nyr.eq.nyr_max)
    panres@gsnPanelScalePlotIndex = tt(0)
    delete(tt)
  end if
  if (nsim.le.12) then
    lp = (/nsim,1/)
  else
    lp = (/nrow,ncol/)   ;(/nsim/2+1,nsim/8+1/)  
  end if

  panres@txString = "NE Brazil Rainfall (Monthly, "+title_neb+")"
  gsn_panel2(wks_neb,xyneb,lp,panres)    
  
  panres@txString = "All India (Monthly, "+title_allindia+")"
  gsn_panel2(wks_allindia,xyallindia,lp,panres)  
  
  panres@txString = "East African (Monthly, "+title_eafrican+")"
  gsn_panel2(wks_eafrican,xyeafrican,lp,panres)       
  
  panres@txString = "East Asian Monsoon (Monthly, "+title_easian+")"
  gsn_panel2(wks_easian,xyeasian,lp,panres)       
  
  panres@txString = "England & Wales Precipitation (Monthly, "+title_hadewp+")"
  gsn_panel2(wks_hadewp,xyhadewp,lp,panres)
  
  panres@txString = "Sahel Rainfall (Monthly, "+title_sahel+")"
  gsn_panel2(wks_sahel,xysahel,lp,panres)
  
  panres@txString = "Baikal Rainfall (Monthly, "+title_baikal+")"
  gsn_panel2(wks_baikal,xybaikal,lp,panres)

  panres@txString = "Iberian Rainfall (Monthly, "+title_iberia+")"
  gsn_panel2(wks_iberia,xyiberia,lp,panres)
  print("Finished: pr.indices.ncl")  
end
