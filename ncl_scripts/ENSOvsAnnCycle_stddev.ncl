;
; A routine to make a plots of the stddev in various simulations of both ENSO and the Annual Cycle
;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"      ; These four libraries are automatically
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"       ; loaded from NCL V6.4.0 onward.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"   ; No need for user to explicitly load.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "ncl_scripts/functions.ncl"
load "ncl_scripts/cvdp_data.functions.ncl"

;Plot settings...
PRINT_SDS_RAW=False
PRINT_SDS_ANOM=False
PRINT_ANNCYC_VALUES=False
PCT_CHANGES=False
BOX_PLOT=True
SCATTER_PLOT=True
ctl_expt="piControl" ;set the baseline - probably either piControl or historical
TAKE_END_OF_1pctCO2=True
data_dir="data"

USE_OLD_APPROACH=True

;list of expts and gcms
expts=(/"piControl","midHolocene","lgm","historical","1pctCO2","past1000"/)
expt_colors=(/"black","green","blue","black","red","orange"/)
gcms=(/"bcc-csm1-1","CCSM4","CNRM-CM5","COSMOS-ASO","CSIRO-Mk3-6-0","CSIRO-Mk3L-1-2","EC-EARTH-2-2","FGOALS-g2","FGOALS-gl","FGOALS-s2","GISS-E2-R","HadCM3","HadGEM2-CC","HadGEM2-ES","IPSL-CM5A-LR","MIROC-ESM","MPI-ESM-P","MRI-CGCM3"/)
;KCM1-2-2 appears to show absolutely no seasonal variability. I don't like that and will need to explore further. Removing for the moment
;gcms=(/"bcc-csm1-1","CCSM4","CNRM-CM5","COSMOS-ASO","CSIRO-Mk3-6-0","CSIRO-Mk3L-1-2","EC-EARTH-2-2","FGOALS-g2","FGOALS-gl","FGOALS-s2","GISS-E2-R","HadCM3","HadGEM2-CC","HadGEM2-ES","IPSL-CM5A-LR","KCM1-2-2","MIROC-ESM","MPI-ESM-P","MRI-CGCM3"/)


if USE_OLD_APPROACH then
  ;Search through directories to read in and store the std devs
  data_output_files = gcms+".output/*.cvdp_data.[0-9]*-*.nc"
  ncfiles=systemfunc("ls "+str_join(data_output_files," "))
  enso_sds=new(dimsizes(ncfiles),"float")
  names=new(dimsizes(ncfiles),"string")
  anncyc_sds=new(dimsizes(ncfiles),"float")
  
  if PRINT_ANNCYC_VALUES then
    print(" ")
    print("Annual Cycle")
    print("model,J,F,M,A,M,J,J,A,S,O,N,D")
  end if


  do i=0,dimsizes(ncfiles)-1
    dummy_strs1=str_split(ncfiles(i),"/")
    dummy_strs2=str_split(dummy_strs1(1),".")
    names(i)=dummy_strs2(0)
    if isfilepresent(ncfiles(i))
      fil=addfile(ncfiles(i),"r")
      if isfilevar(fil,"nino34").and.isfilevaratt(fil,"nino34","AnnCycle") then
        nino34=fil->nino34
        if TAKE_END_OF_1pctCO2.and.isStrSubset(ncfiles(i),"1pctCO2") then
          final_40yrs=nino34(dimsizes(nino34)-40*12-1:dimsizes(nino34)-1)
          anncyc_orig=nino34@AnnCycle
          anncyc = new(12,typeof(nino34),getFillValue(nino34))
          do nmo=0,11
            anncyc(nmo) = dim_avg(final_40yrs(nmo:40*12-1:12))
          end do
          anncyc=anncyc+anncyc_orig
          enso_sds(i)=stddev(dtrend(final_40yrs,True))
        else
          enso_sds(i)=stddev(dtrend(nino34,True))
          anncyc=nino34@AnnCycle
        end if
        if PRINT_ANNCYC_VALUES then
          for_printing=new(13,"string")
          for_printing(0)=names(i)
          for_printing(1:12)=anncyc
          for_printing=for_printing+","
          print(str_concat(for_printing))
          delete(for_printing)
        end if
        anncyc_sds(i)=stddev(anncyc)
        delete([/nino34,anncyc/])
      else
        enso_sds(i)=enso_sds@_FillValue
        anncyc_sds(i)=anncyc_sds@_FillValue
      end if
      delete(fil)
    end if
    delete([/dummy_strs1,dummy_strs2/])
  end do

  if PRINT_SDS_RAW then
    print(" ")
    print("Standard deviations for ENSO (first) and Annual Cycle (second)")
    print("model,enso sd,annual cycle sd")
    do i=0,dimsizes(ncfiles)-1
      print(names(i)+","+enso_sds(i)+","+anncyc_sds(i))
    end do
  end if

;compute the changes
  do gcm_i=0,dimsizes(gcms)-1
    these_runs=str_match_ind(names,gcms(gcm_i))
    ctl=str_match_ind(names(these_runs),ctl_expt)
    if all(ismissing(ctl)) then 
      enso_sds(these_runs)=enso_sds@_FillValue
      anncyc_sds(these_runs)=anncyc_sds@_FillValue
    else
      if dimsizes(ctl).gt.1 then
        enso_ctl=enso_sds(these_runs(ctl(0)))
        anncyc_ctl=anncyc_sds(these_runs(ctl(0)))
      else
        enso_ctl=enso_sds(these_runs(ctl))
        anncyc_ctl=anncyc_sds(these_runs(ctl))
      end if  
      if PCT_CHANGES then
        enso_sds(these_runs)=100.*(enso_sds(these_runs)-enso_ctl)/enso_ctl
        anncyc_sds(these_runs)=100.*(anncyc_sds(these_runs)-anncyc_ctl)/anncyc_ctl
      else
        enso_sds(these_runs)=enso_sds(these_runs)-enso_ctl
        anncyc_sds(these_runs)=anncyc_sds(these_runs)-anncyc_ctl
      end if
    end if
    delete([/these_runs,ctl/])
  end do

  if PRINT_SDS_ANOM then
    print(" ")
    print("Change in standard deviations for ENSO (first) and Annual Cycle (second)")
    print("model, enso sd diff, annual cycle sd diff")
    do i=0,dimsizes(ncfiles)-1
      print(names(i)+","+enso_sds(i)+","+anncyc_sds(i))
    end do
  end if

  if BOX_PLOT then
    x = ispan(1,dimsizes(expts),1)
    y_ann = new((/dimsizes(expts),5/),float)
    y_enso=y_ann
    do j = 0,dimsizes(expts)-1
      these_runs=str_match_ind(names,expts(j))
      g = stat_dispersion(anncyc_sds(these_runs),False)
      y_ann(j,0) = g(2)  ; Tamanho da haste inferior.
      y_ann(j,1) = g(7)  ; Primeiro quartil.
      y_ann(j,2) = g(8)  ; Mediana.
      y_ann(j,3) = g(11) ; Terceiro quartil.
      y_ann(j,4) = g(14) ; Tamanho da haste superior.
      g = stat_dispersion(enso_sds(these_runs),False)
      y_enso(j,0) = g(2)  ; Tamanho da haste inferior.
      y_enso(j,1) = g(7)  ; Primeiro quartil.
      y_enso(j,2) = g(8)  ; Mediana.
      y_enso(j,3) = g(11) ; Terceiro quartil.
      y_enso(j,4) = g(14) ; Tamanho da haste superior.
      delete(these_runs)
    end do

    wks = gsn_open_wks("png","ENSOvsAnnCycle_stddev_boxplot")
    res                 = True         ; PersonalizaÃ§Ã£o do boxplot.
    res@tmXBLabels      = expts      ; RÃ³tulos do eixo x.
    res@tmYLMinorOn = True
    res@tmYRBorderOn    = True         ; Habilita a borda do eixo y direito (YR). 
    res@tmXTBorderOn    = True         ; Habilita a borda do eixo x superior (XB). 
    res@tmXBLabelAngleF = 90.
    llres                   = True
    llres@gsLineThicknessF  = 2.5   
    opti           = True 
    opti@boxWidth  = 0.35 
    opti@boxColors = expt_colors

    ;mnmxint = nice_mnmxintvl(min(y_enso(:,0)),max(y_enso(:,4)),18, True)
    if PCT_CHANGES then
      res@tmYLMode        = "Explicit"	   ; Eixo y esquerdo (YL) de forma manual.
      res@trYMinF         = -50
      res@trYMaxF         = 35
      levs=ispan(-50,35,5)
      res@tmYLValues = levs
      res@tmYLLabels = levs
      res@tiYAxisString   = "ENSO Amplitude Change (%)"
    else
      res@tiYAxisString   = "ENSO Amplitude Change"
    end if
    plot1 = boxplot(wks,x,y_enso,opti,res,llres)

    if PCT_CHANGES then
      res@trYMinF = 2*res@trYMinF
      res@trYMaxF =2*res@trYMaxF
      res@tmYLValues = 2*levs
      res@tmYLLabels = 2*levs
      res@tiYAxisString   = "Annual Cycle Amplitude Change (%)"
      delete([/levs/])
    else
      res@tiYAxisString   = "Annual Cycle Amplitude Change"
    end if
    plot2 = boxplot(wks,x,y_ann,opti,res,llres)

    resP=True
    resP@gsnMaximize=True
    resP@gsnPanelYWhiteSpacePercent = 20.0
    gsn_panel(wks,(/plot1,plot2/),(/1,2/),True)
    delete([/wks,resP,res,plot1,plot2,opti,x,y_ann,y_enso,llres,g/])
  end if

  if SCATTER_PLOT then
    wks = gsn_open_wks("png","ENSOvsAnnCycle_stddev_scatterplot")
    res                   = True                     ; plot mods desired
    res@tiMainString      = "Scatter Plot"           ; add title
    res@xyMarkLineModes   = "Markers"                ; choose which have markers
    res@xyMarkers         =  16                      ; choose type of marker  
    res@xyMonoMarkerColor = False
    res@xyMarkerSizeF     = 0.01                     ; Marker size (default 0.01)
    res@tmLabelAutoStride = True                     ; nice tick mark labels
    if PCT_CHANGES then
      res@tmYLMode        = "Explicit"
      res@trYMinF         = -50
      res@trYMaxF         = 35
      levs=ispan(-50,35,5)
      res@tmYLValues = levs
      res@tmYLLabels = levs
      res@tiYAxisString   = "ENSO Amplitude Change (%)"
      res@trXMinF = 2*res@trYMinF
      res@trXMaxF =2*res@trYMaxF
      res@tmXBValues = 2*levs
      res@tmXBLabels = 2*levs
      res@tiXAxisString   = "Annual Cycle Amplitude Change (%)"
    else
      res@tiYAxisString   = "ENSO Amplitude Change"
      res@tiXAxisString   = "Annual Cycle Amplitude Change"
    end if
    res@xyMarkerColors  = new(dimsizes(names),string)
    res@xyMarkerColors(:) = "transparent"
    do j=0,dimsizes(expts)-1
      print(expts(j)+", #="+dimsizes(str_match_ind(names,expts(j)))+", of color "+expt_colors(j))
      res@xyMarkerColors(str_match_ind(names,expts(j)))=(/expt_colors(j)/)
    end do
    print(res@xyMarkerColors(0:6))
    plot  = gsn_csm_xy (wks,anncyc_sds,enso_sds,res) ; create plot
  end if

else
; use my new approach with the cvdp_data functions....

  if BOX_PLOT then
    x = ispan(1,dimsizes(expts)-1,1)
    y_ann = new((/dimsizes(expts)-1,5/),float)
    y_enso=y_ann
    do j = 0,dimsizes(expts)-2
      fnames_both=find_pair_files_wVar(data_dir,expts(j+1),expts(0),"nino34")
      ;print(fnames_both)
      opt=True
      opt@returnAnnCycle=True
      enso_sd_diffs=new(dimsizes(fnames_both(:,0)),float)
      anncyc_sd_diffs=enso_sd_diffs
      do i=0,dimsizes(fnames_both(:,0))-1
        run=stat4_ts_var(fnames_both(i,0),"nino34",opt)
        ctl=stat4_ts_var(fnames_both(i,1),"nino34",opt)
        if PCT_CHANGES then
          enso_sd_diffs(i)=100*(run(1)-ctl(1))/ctl(1)
          anncyc_sd_diffs(i)=100*(stddev(run@AnnCycle)-stddev(ctl@AnnCycle))/stddev(ctl@AnnCycle)
        else
          enso_sd_diffs(i)=run(1)-ctl(1)
          anncyc_sd_diffs(i)=stddev(run@AnnCycle)-stddev(ctl@AnnCycle)
        end if
      end do
      g = stat_dispersion(anncyc_sd_diffs,False)
      y_ann(j,0) = g(2)  ; Tamanho da haste inferior.
      y_ann(j,1) = g(7)  ; Primeiro quartil.
      y_ann(j,2) = g(8)  ; Mediana.
      y_ann(j,3) = g(11) ; Terceiro quartil.
      y_ann(j,4) = g(14) ; Tamanho da haste superior.
      g = stat_dispersion(enso_sd_diffs,False)
      y_enso(j,0) = g(2)  ; Tamanho da haste inferior.
      y_enso(j,1) = g(7)  ; Primeiro quartil.
      y_enso(j,2) = g(8)  ; Mediana.
      y_enso(j,3) = g(11) ; Terceiro quartil.
      y_enso(j,4) = g(14) ; Tamanho da haste superior.
      delete([/fnames_both,enso_sd_diffs,anncyc_sd_diffs,g,opt/])
    end do
    wks = gsn_open_wks("png","ENSOvsAnnCycle_stddev_boxplot.new")
    res                 = True         ; PersonalizaÃ§Ã£o do boxplot.
    res@tmXBLabels      = expts(1:)      ; RÃ³tulos do eixo x.
    res@tmYLMinorOn = True
    res@tmYRBorderOn    = True         ; Habilita a borda do eixo y direito (YR). 
    res@tmXTBorderOn    = True         ; Habilita a borda do eixo x superior (XB). 
    res@tmXBLabelAngleF = 90.
    llres                   = True
    llres@gsLineThicknessF  = 2.5   
    opti           = True 
    opti@boxWidth  = 0.35 
    opti@boxColors = expt_colors(1:)

    ;mnmxint = nice_mnmxintvl(min(y_enso(:,0)),max(y_enso(:,4)),18, True)
    if PCT_CHANGES then
      res@tmYLMode        = "Explicit"	   ; Eixo y esquerdo (YL) de forma manual.
      res@trYMinF         = -50
      res@trYMaxF         = 35
      levs=ispan(-50,35,5)
      res@tmYLValues = levs
      res@tmYLLabels = levs
      res@tiYAxisString   = "ENSO Amplitude Change (%)"
    else
      res@tiYAxisString   = "ENSO Amplitude Change"
    end if
    plot1 = boxplot(wks,x,y_enso,opti,res,llres)

    if PCT_CHANGES then
      res@trYMinF = 2*res@trYMinF
      res@trYMaxF =2*res@trYMaxF
      res@tmYLValues = 2*levs
      res@tmYLLabels = 2*levs
      res@tiYAxisString   = "Annual Cycle Amplitude Change (%)"
      delete([/levs/])
    else
      res@tiYAxisString   = "Annual Cycle Amplitude Change"
    end if
    plot2 = boxplot(wks,x,y_ann,opti,res,llres)

    resP=True
    resP@gsnMaximize=True
    resP@gsnPanelYWhiteSpacePercent = 20.0
    gsn_panel(wks,(/plot1,plot2/),(/1,2/),True)
    delete([/wks,resP,res,plot1,plot2,opti,x,y_ann,y_enso,llres/])

  end if
end if
