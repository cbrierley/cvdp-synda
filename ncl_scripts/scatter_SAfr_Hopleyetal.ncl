;This script was used to create sufficient figures to populate a research publication (hopefully Brierley & Wainer)
;It makes full use of the programs in cvdp_data.functions.ncl to plot figures solely from the output data of cvdp (without needing recomputation).
;As the novelty of the manuscript is the new Atlantic modes provided by cpl.sst, it focuses on those variables.
; It makes 1 table (showing simulations used) and many figures. 

;Set up paths
data_dir="data"
;load some functions (first 4 get loaded automatically on newer versions of NCL
load "ncl_scripts/functions.ncl"
load "ncl_scripts/cvdp_data.functions.ncl"

expts=(/"piControl","historical","midHolocene","lgm","1pctCO2","past1000"/)
expts_color=(/"grey24","black","darkgreen","dodgerblue4","firebrick","darkorchid"/)
gcms=(/"bcc-csm1-1","CCSM4","CNRM-CM5","COSMOS-ASO","CSIRO-Mk3-6-0","CSIRO-Mk3L-1-2","EC-EARTH-2-2","FGOALS-g2","FGOALS-gl","FGOALS-s2","GISS-E2-R","HadCM3","HadGEM2-CC","HadGEM2-ES","IPSL-CM5A-LR","MIROC-ESM","MPI-ESM-P","MRI-CGCM3"/)

;Set some plot switches
PLOT_SCATTER=True ;if True, create a scatterplot of the change in variability amplitude vs the change in global mean temperature change.
SCATTER_PCT_CHANGES=False ;if True, the two scatterplots present the changes as percentages.
SINGLE_SEASON=True
SEASON="DJF";
VERBOSE=True ;if True, provides a modicum of more information about whats goign on in the plots - used for debugging mainly.

;yaxis_timeseries_netcdf_name="nino34"
;yaxis_name="Nino 3.4 amplitude change"
yaxis_field_netcdf_name="pr_spatialstddev_djf"
yaxis_name="Change in Std Dev. DJF rainfall"
xaxis_field_netcdf_name="tas_spatialmean_djf"
xaxis_name="Change in DJF mean temperature"



fnames_pictl=find_files_wVar(data_dir,"piControl",yaxis_field_netcdf_name)
num_pictl=dimsizes(fnames_pictl)
d_stddev=new(num_pictl*3,float);array to hold variability change
d_meanstate=new(num_pictl*3,float);array to hold mean state change
ts_opt=True
if SINGLE_SEASON then
  ts_opt@SEASON=SEASON
  mean_field=str_sub_str(xaxis_field_netcdf_name,"ann",str_switch(SEASON))
else
  mean_field=xaxis_field_netcdf_name
end if

;only take the end of the 1pctCO2 runs...
ts_1pctCO2=ts_opt
ts_1pctCO2@renorm_climo=True
ts_1pctCO2@subset=True
ts_1pctCO2@subset_syear=-39
ts_1pctCO2@subset_eyear=0
ts_1pctCO2@renorm_climo_syear=-39
ts_1pctCO2@renorm_climo_eyear=0
ts_1pctCO2@DETREND=True

;collect all the data
do expt_i=0,2
  if expts(expt_i+2).eq."1pctCO2" then
    ts_a=ts_1pctCO2
  else
    ts_a=ts_opt
  end if
  fnames_both=find_pair_files_wVar(data_dir,expts(expt_i+2),"piControl",(/xaxis_field_netcdf_name,yaxis_field_netcdf_name/))
  do gcm_i=0,dimsizes(fnames_both(:,0))-1
    sd_a=extract_latlon_areastat(fnames_both(gcm_i,0),yaxis_field_netcdf_name,(/-30,-20./),(/22,32/),"mean")
    sd_b=extract_latlon_areastat(fnames_both(gcm_i,1),yaxis_field_netcdf_name,(/-30,-20./),(/22,32/),"mean")
    areaavg_a=extract_latlon_areastat(fnames_both(gcm_i,0),mean_field,(/-30,-20./),(/22,32/),"mean")
    areaavg_b=extract_latlon_areastat(fnames_both(gcm_i,1),mean_field,(/-30.,-20./),(/22,32/),"mean")
    if SCATTER_PCT_CHANGES then
      d_stddev(expt_i*num_pictl+gcm_i)=100.*(sd_a-sd_b)/sd_b
      d_meanstate(expt_i*num_pictl+gcm_i)=100.*(areaavg_a-areaavg_b)/areaavg_b
    else
      d_stddev(expt_i*num_pictl+gcm_i)=sd_a-sd_b
      d_meanstate(expt_i*num_pictl+gcm_i)=areaavg_a-areaavg_b
    end if
    delete([/sd_a,sd_b,areaavg_a,areaavg_b/])
  end do
  delete(fnames_both)
end do 

if VERBOSE then
  print("I have looked at the change in "+yaxis_field_netcdf_name+" against the change in "+xaxis_field_netcdf_name)
  if SINGLE_SEASON then
    print("The season option was set to: "+SEASON)
  else
    print("The season option was set to "+SINGLE_SEASON)
  end if
  ;print(d_stddev)
  ;print(d_meanstate)
  r = escorc(d_stddev,d_meanstate)                ; Pearson correlation
  df = num(.not.ismissing(d_stddev))-2                ; n=11
  z = 0.5*log((1+r)/(1-r))  ; z-statistic
  se   = 1.0/sqrt(df-1)                       ; standard error of z-statistic
  zlow = z - 1.96*se                ; 95%  (2.58 for 99%)
  zhi  = z + 1.96*se                 
  rlow = (exp(2*zlow)-1)/(exp(2*zlow)+1)
  rhi  = (exp(2*zhi )-1)/(exp(2*zhi )+1)
  print("The correlation is "+r+" ("+rlow+"-"+rhi+")")
end if

;create the scatter plot
if PLOT_SCATTER then
  wks = gsn_open_wks("pdf","scatter_SAfr_Hopleyetal")
  res                   = True                     ; plot mods desired
  res@gsnDraw = False
  res@gsnFrame = False                     ; plot mods desired
  res@tiMainString      = " "           ; add title
  res@xyMarkLineModes   = "Markers"                ; choose which have markers
  res@xyMarkers         =  16                      ; choose type of marker  
  res@xyMonoMarkerColor = True
  res@xyMarkerSizeF     = 0.01                     ; Marker size (default 0.01)
  res@gsnYRefLine=0.0
  res@gsnXRefLine=0.0
  res@tmLabelAutoStride = True                     ; nice tick mark labels
  res@tiYAxisString   = yaxis_name
  res@tiXAxisString   = xaxis_name
  if SCATTER_PCT_CHANGES then
    res@tiYAxisString   = res@tiYAxisString+" (%)"
    res@tiXAxisString   = res@tiXAxisString+" (%)"
  end if
  res@trXMaxF=max(d_meanstate)
  res@trXMinF=min(d_meanstate)
  res@trYMaxF=max(d_stddev)
  res@trYMinF=min(d_stddev)
  
  res@xyMarkerColor =  expts_color(2) 
  plot  = gsn_csm_xy (wks,d_meanstate(0:num_pictl-1),d_stddev(0:num_pictl-1),res) ; create plot
  res@xyMarkerColor =  expts_color(3) 
  oplot1  = gsn_csm_xy (wks,d_meanstate(num_pictl:2*num_pictl-1),d_stddev(num_pictl:2*num_pictl-1),res)
  res@xyMarkerColor =  expts_color(4) 
  oplot2  = gsn_csm_xy (wks,d_meanstate(2*num_pictl:3*num_pictl-1),d_stddev(2*num_pictl:3*num_pictl-1),res)
  overlay(plot,oplot1)
  overlay(plot,oplot2)
  draw(plot)
  frame(wks)
  delete(wks)
end if ;PLOT_SCATTER
