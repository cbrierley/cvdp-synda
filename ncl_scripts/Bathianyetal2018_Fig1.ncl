;This script looks at replicating the starting figure of a 2018 paper looking at future climate variability.

;This is not a perfrect replicate as they look at all future runs and go from 1850-2100.
;Here we will only take rcp85 (end of) away from hist (1981-2010)

;REFERENCE
;Climate models predict increasing temperature variability in poor countries
;Sebastian Bathiany, Vasilis Dakos, Marten Scheffer and Timothy M. Lenton
;Science Advances, 02 May 2018, Vol. 4, no. 5, eaar5809, DOI: 10.1126/sciadv.aar5809 

;It uses plotCVDPcomparisonMaps in cvdp_data.functions.ncl to do this.

;First load the ncl scripts
load "~/ncl/cvdp-synda/ncl_scripts/cvdp_data.functions.ncl"

data_dir="data" ;Point the directory containing the output from the CVDP

opt=True
opt@cnFillPalette="cmp_b2r" ;change to a blue-red color bar
opt@CONSISTENCY=True
opt@PERCENTAGE=True
;Use the routines in cvdp_data.functions.ncl to create the plot
plotDiffEnsMnMaps(data_dir,"rcp85","historical",(/"tas_spatialstddev_djf","tas_spatialstddev_jja","tas_spatialstddev_ann"/),"Bathianyetal2018_Fig1",opt,False)
