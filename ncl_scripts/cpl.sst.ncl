; Calculates modes of variability for the tropical Atlantic: patterns, timeseries, precipiation regressions and spectra.
;
; Variables used: ts, pr
;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$CVDP_SCRIPTS/functions.ncl"

begin
  print("Starting: cpl.sst.ncl")
  
  OUTDIR      = getenv("OUTDIR") 
  SCALE_TIMESERIES = getenv("SCALE_TIMESERIES")  
  OUTPUT_DATA      = getenv("OUTPUT_DATA")  
  PNG_SCALE        = tofloat(getenv("PNG_SCALE"))
  OPT_CLIMO        = getenv("OPT_CLIMO")
  CLIMO_SYEAR      = toint(getenv("CLIMO_SYEAR"))
  CLIMO_EYEAR      = toint(getenv("CLIMO_EYEAR"))
  OUTPUT_TYPE      = getenv("OUTPUT_TYPE") 
  COLORMAP         = getenv("COLORMAP")  
  
  nsim = numAsciiRow(OUTDIR+"namelist_byvar/namelist_ts")
  na = asciiread(OUTDIR+"namelist_byvar/namelist_ts",(/nsim/),"string")
  names = new(nsim,"string")
  paths = new(nsim,"string")
  syear = new(nsim,"integer",-999)
  eyear = new(nsim,"integer",-999)
  delim = "|"

  do gg = 0,nsim-1
    names(gg) = str_strip(str_get_field(na(gg),1,delim))
    paths(gg) = str_strip(str_get_field(na(gg),2,delim))
    syear(gg) = stringtointeger(str_strip(str_get_field(na(gg),3,delim)))
    eyear(gg) = stringtointeger(str_strip(str_get_field(na(gg),4,delim)))
  end do
  nyr = eyear-syear+1
  nyr_max = max(nyr)

;---------PR Regressions coding-------------------------------------------------
  nsim_pr = numAsciiRow(OUTDIR+"namelist_byvar/namelist_prect")
  na_pr = asciiread(OUTDIR+"namelist_byvar/namelist_prect",(/nsim_pr/),"string")
  names_pr = new(nsim_pr,"string")
  paths_pr = new(nsim_pr,"string")
  syear_pr = new(nsim_pr,"integer",-999)
  eyear_pr = new(nsim_pr,"integer",-999)

  do gg = 0,nsim_pr-1
    names_pr(gg) = str_strip(str_get_field(na_pr(gg),1,delim))
    paths_pr(gg) = str_strip(str_get_field(na_pr(gg),2,delim))
    syear_pr(gg) = stringtointeger(str_strip(str_get_field(na_pr(gg),3,delim)))
    eyear_pr(gg) = stringtointeger(str_strip(str_get_field(na_pr(gg),4,delim)))
  end do
  delete(na_pr)
  nyr_pr = eyear_pr-syear_pr+1  
;-------------------------------------------------------------------------------------------------
  pi=4.*atan(1.0)
  rad=(pi/180.)
  
  wks_type = OUTPUT_TYPE
  if (wks_type.eq."png") then
    wks_type@wkWidth = 1500*PNG_SCALE
    wks_type@wkHeight = 1500*PNG_SCALE
  end if
  wks_atl3 = gsn_open_wks(wks_type,getenv("OUTDIR")+"atl3")
  wks_atl3_spec = gsn_open_wks(wks_type,getenv("OUTDIR")+"atl3.powspec")
  wks_atl3_pr = gsn_open_wks(wks_type,getenv("OUTDIR")+"atl3.prreg")
  wks_amm = gsn_open_wks(wks_type,getenv("OUTDIR")+"amm")
  wks_amm_spec = gsn_open_wks(wks_type,getenv("OUTDIR")+"amm.powspec")
  wks_amm_pr = gsn_open_wks(wks_type,getenv("OUTDIR")+"amm.prreg")
  wks_sasd = gsn_open_wks(wks_type,getenv("OUTDIR")+"sasd")
  wks_sasd_spec = gsn_open_wks(wks_type,getenv("OUTDIR")+"sasd.powspec")
  wks_sasd_pr = gsn_open_wks(wks_type,getenv("OUTDIR")+"sasd.prreg")
  wks_iod = gsn_open_wks(wks_type,getenv("OUTDIR")+"iod")
  wks_iod_spec = gsn_open_wks(wks_type,getenv("OUTDIR")+"iod.powspec")
  wks_iod_pr = gsn_open_wks(wks_type,getenv("OUTDIR")+"iod.prreg")
  ;need a single workstation to hold all timeseries as NCL can't cope with >15
  wks_ts = gsn_open_wks(wks_type,getenv("OUTDIR")+"cpl.sst.timeseries")  

  if (COLORMAP.eq."0") then
    gsn_define_colormap(wks_atl3,"ncl_default")   
    gsn_define_colormap(wks_atl3_spec,"cb_9step")
    gsn_define_colormap(wks_ts,"ncl_default")
    gsn_define_colormap(wks_atl3_pr,"MPL_BrBG")  
    gsn_define_colormap(wks_amm,"ncl_default")   
    gsn_define_colormap(wks_amm_spec,"cb_9step")
    gsn_define_colormap(wks_amm_pr,"MPL_BrBG")  
    gsn_define_colormap(wks_sasd,"ncl_default")   
    gsn_define_colormap(wks_sasd_spec,"cb_9step")
    gsn_define_colormap(wks_sasd_pr,"MPL_BrBG")  
    gsn_define_colormap(wks_iod,"ncl_default")   
    gsn_define_colormap(wks_iod_spec,"cb_9step")
    gsn_define_colormap(wks_iod_pr,"MPL_BrBG")  
  end if
  if (COLORMAP.eq."1") then
    gsn_define_colormap(wks_atl3,"BlueDarkRed18")   
    gsn_define_colormap(wks_atl3_spec,"cb_9step")
    gsn_define_colormap(wks_atl3_pr,"BrownBlue12")    
    gsn_define_colormap(wks_amm,"BlueDarkRed18")   
    gsn_define_colormap(wks_amm_spec,"cb_9step")
    gsn_define_colormap(wks_amm_pr,"BrownBlue12")    
    gsn_define_colormap(wks_sasd,"BlueDarkRed18")   
    gsn_define_colormap(wks_sasd_spec,"cb_9step")
    gsn_define_colormap(wks_sasd_pr,"BrownBlue12")    
    gsn_define_colormap(wks_iod,"BlueDarkRed18")   
    gsn_define_colormap(wks_iod_spec,"cb_9step")
    gsn_define_colormap(wks_iod_pr,"BrownBlue12")    
  end if
  map1 = new(nsim,"graphic")  
  pspec1 = new(nsim,"graphic")
  xyplot1 = new(nsim,"graphic")
  reg_atl3_pr = new(nsim,"graphic")  
  map2 = new(nsim,"graphic")  
  pspec2 = new(nsim,"graphic")
  xyplot2 = new(nsim,"graphic")
  reg_amm_pr = new(nsim,"graphic")  
  map3 = new(nsim,"graphic")  
  pspec3 = new(nsim,"graphic")
  xyplot3 = new(nsim,"graphic")
  reg_sasd_pr = new(nsim,"graphic")  
  map4 = new(nsim,"graphic")  
  pspec4 = new(nsim,"graphic")
  xyplot4 = new(nsim,"graphic")
  reg_iod_pr = new(nsim,"graphic")  
  if (isfilepresent2("obs_ts")) then
    pspec_obs1 = new(nsim,"graphic")
    pspec_obs2 = new(nsim,"graphic")
    pspec_obs3 = new(nsim,"graphic")
    pspec_obs4 = new(nsim,"graphic")
  end if

  do ee = 0,nsim-1
    sst = data_read_in(paths(ee),"TS",syear(ee),eyear(ee))    ; read in data, orient lats/lons correctly, set time coordinate variable up
    if (isatt(sst,"is_all_missing").or.nyr(ee).lt.40) then
      delete(sst)
      continue
    end if
    sst = where(sst.le.-1.8,-1.8,sst)    ; set all values below -1.8 to -1.8
    d = addfile("$NCARG_ROOT/lib/ncarg/data/cdf/landsea.nc","r")   ; mask out land (this is redundant for data that is already masked)
    basemap = d->LSMASK
    lsm = landsea_mask(basemap,sst&lat,sst&lon)
    sst = mask(sst,conform(sst,lsm,(/1,2/)).ge.1,False)
    delete([/lsm,basemap/])
    delete(d)  

    if (OPT_CLIMO.eq."Full") then
      climo = clmMonTLL(sst)                 
      sst = rmMonAnnCycTLL(sst)
    else
      if CLIMO_SYEAR.lt.0.and.CLIMO_EYEAR.le.0 then
        ;if negative, the climatology is set relative to the run end.
        climo_syear=eyear(ee)+CLIMO_SYEAR
        climo_eyear=eyear(ee)+CLIMO_EYEAR
      else
        climo_syear=CLIMO_SYEAR
        climo_eyear=CLIMO_EYEAR
      end if                   
      check_custom_climo(names(ee),syear(ee),eyear(ee),climo_syear,climo_eyear)
      temp_arr = sst
      delete(temp_arr&time)
      temp_arr&time = cd_calendar(sst&time,-1)
      climo = clmMonTLL(temp_arr({climo_syear*100+1:climo_eyear*100+12},:,:))                 
      delete(temp_arr)
      sst   = calcMonAnomTLL(sst,climo) 
    end if
    sst = (/ dtrend_msg_n(ispan(0,nyr(ee)*12-1,1),sst,False,False,0) /)

    coswgt=cos(rad*sst&lat)
    coswgt!0 = "lat"
    coswgt&lat= sst&lat    

    ;for domains stretching over the Greenwich Meridian, flipped longitudes are much easier
    sst_flip=lonFlip(sst)
    climo_flip=lonFlip(climo)

    ;Equatorial Mode
    llats = -3.
    llatn = 3.
    llonw = -20.
    llone = 0.
    atl3 = wgt_areaave_Wrap(sst_flip(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    atl3@comment_cvdp = "area average domain ("+llats+":"+llatn+"N, "+llonw+":"+llone+"E)"
    atl3@units = sst@units
    atl3@long_name = "atl3 timeseries (monthly)"       
    atl3_climo = wgt_areaave_Wrap(climo_flip(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    atl3@AnnCycle = (/atl3_climo/)
    delete(atl3_climo)
    atl3_patt = sst(0,:,:)
    atl3_patt = (/ regCoef(atl3,sst(lat|:,lon|:,time|:))  /) 
    do gg = 0,2
      atl3_patt = (/ smth9(atl3_patt,0.5,0.25,True) /)
    end do
    atl3_patt@syear = syear(ee)
    atl3_patt@eyear = eyear(ee) 
    
    ;Meridional Mode
    amm=atl3
    amm_naa = wgt_areaave_Wrap(sst_flip(:,{5.:15.},{-50.:-20.}),coswgt({5.:15.}),1.0,0)
    amm_saa = wgt_areaave_Wrap(sst_flip(:,{-15.:-5.},{-20.:10.}),coswgt({-15.:-5.}),1.0,0)
    amm=(/amm_naa - amm_saa/)
    copy_VarMeta(amm_naa,amm)
    delete([/amm_naa,amm_saa/])
    amm@comment_cvdp = "difference between area average domains (5-15N,50-20W) - (15-5S,20W-10E)"
    amm@units = sst@units
    amm@long_name = "Atl. Merid. Mode timeseries (monthly)"       
    amm_climo = wgt_areaave_Wrap(climo_flip(:,{llats:llatn},{llonw:llone}),coswgt({llats:llatn}),1.0,0)  
    amm@AnnCycle = (/amm_climo/)
    amm!0 = "time"
    amm&time = sst&time
    delete(amm_climo)
    amm_patt = sst(0,:,:)
    amm_patt = (/ regCoef(amm,sst(lat|:,lon|:,time|:))  /) 
    do gg = 0,2
      amm_patt = (/ smth9(amm_patt,0.5,0.25,True) /)
    end do
    amm_patt@syear = syear(ee)
    amm_patt@eyear = eyear(ee) 

    ; Indian Ocean Dipole Index http://www.bom.gov.au/climate/IOD/about_IOD.shtml
    iod = wgt_areaave(sst(:,{-10.:10.},{50.:70.}),coswgt({-10.:10.}),1.0,0) - wgt_areaave(sst(:,{-10.:0.},{90.:110.}),coswgt({-10.:0.}),1.0,0)  
    iod@comment_cvdp = "area average domain (-10:10N, 50:70E) - (-10:0N, 90:110E)"
    copy_VarCoords(atl3,iod)
    iod@units = sst@units
    iod@long_name = "Indian Ocean Dipole Index (monthly)"       
    iod_climo = wgt_areaave(climo(:,{-10.:10.},{50.:70.}),coswgt({-10.:10.}),1.0,0) - wgt_areaave(climo(:,{-10.:0.},{90.:110.}),coswgt({-10.:0.}),1.0,0)  
    iod@AnnCycle = (/iod_climo/)
    iod!0 = "time"
    iod&time = sst&time
    delete(iod_climo)
    iod_patt = sst(0,:,:)
    iod_patt = (/ regCoef(iod,sst(lat|:,lon|:,time|:))  /) 
    do gg = 0,2
      iod_patt = (/ smth9(iod_patt,0.5,0.25,True) /)
    end do
    iod_patt@syear = syear(ee)
    iod_patt@eyear = eyear(ee) 
    delete(coswgt)

    sst_CW = sst_flip
    sst_CW = SqrtCosWeight(sst_flip)  
    evecv = eofunc(sst_CW({lat|-50:0},{lon|-60:20},time|:),1,75)
    delete(sst_CW)
    sasd = rm_single_dims(eofunc_ts(sst_flip({lat|-50:0},{lon|-60:20},time|:),evecv,False))
    sasd = dim_standardize(sasd,0)

    sasd_patt = sst(0,:,:)
    sasd_patt = sasd_patt@_FillValue
    sasd_patt = (/ regCoef(sasd,sst(lat|:,lon|:,time|:)) /)
    sasd_patt@syear = syear(ee)
    sasd_patt@eyear = eyear(ee)

    sasd!0 = "time"
    sasd&time = sst&time
    sasd@units = "1"
    sasd@pcvar = tofloat(sprintf("%4.1f", evecv@pcvar(0)))+"%"
    delete([/evecv/])
    
    if (.not.ismissing(sasd({0}))) then
      if (sasd_patt({lat|-10.},{lon|2.}).lt.0) then   ; arbitrary attempt to make all plots have the same sign..
        sasd_patt = sasd_patt*-1.
        sasd = sasd*-1.
      end if
    end if
    delete([/climo,climo_flip,sst,sst_flip/])
;---------------------------------------------------------------------------------------------     
    if (OUTPUT_DATA.eq."True") then
      modname = str_sub_str(names(ee)," ","_")
      bc = (/"/","'","(",")"/)
      do gg = 0,dimsizes(bc)-1
        modname = str_sub_str(modname,bc(gg),"_")
      end do
      fn = getenv("OUTDIR")+modname+".cvdp_data.cpl.sst."+syear(ee)+"-"+eyear(ee)+".nc"
      if (.not.isfilepresent2(fn)) then
        z = addfile(fn,"c")
        z@source = "NCAR Climate Analysis Section's Climate Variability Diagnostics Package v"+getenv("VERSION")
        z@notes = "Data from "+names(ee)+" from "+syear(ee)+"-"+eyear(ee)
        if (OPT_CLIMO.eq."Full") then
          z@climatology = syear(ee)+"-"+eyear(ee)+" climatology removed prior to all calculations (other than means)"
        else
          z@climatology = climo_syear+"-"+climo_eyear+" climatology removed prior to all calculations (other than means)"
        end if
        z@Conventions = "CF-1.6"
      else
        z = addfile(fn,"w")
      end if
      z->atl3_pattern_mon = set_varAtts(atl3_patt,"Tropical Atlantic Equatorial Mode spatial pattern (monthly)","","")
      z->atl3_timeseries_mon = set_varAtts(atl3,"Tropical Atlantic Equatorial Mode timeseries (monthly)","oC","")
      z->amm_pattern_mon = set_varAtts(amm_patt,"Tropical Atlantic Meridional Mode spatial pattern (monthly)","","")
      z->amm_timeseries_mon = set_varAtts(amm,"Tropical Atlantic Meridional Mode timeseries (monthly)","oC","")
      z->sasd_pattern_mon = set_varAtts(sasd_patt,"South Atlantic Subtropical Dipole Mode spatial pattern (monthly)","","")
      z->sasd_timeseries_mon = set_varAtts(sasd,"South Atlantic Subtropical Dipole Mode principal component timeseries (monthly)","","")
      z->iod_pattern_mon = set_varAtts(iod_patt,"Indian Ocean Dipole spatial pattern (monthly)","","")
      z->iod_timeseries_mon = set_varAtts(iod,"Indian Ocean Dipole timeseries (monthly)","","")
    end if              
;------------------------------------------------------------------------
    iopt = 0
    jave = (3*nyr(ee))/100
    val1 = .95
    val2 = .99
    if (jave.eq.0) then
      jave = 1
    end if
    pct = 0.1    
    spectra1_mvf = False        ; missing value flag for atl3
    spectra1_mvf_obs = True     ; missing value flag for observational atl3 default
    spectra2_mvf = False        ; missing value flag for amm
    spectra2_mvf_obs = True     ; missing value flag for observational amm default
    spectra3_mvf = False        ; missing value flag for sasd
    spectra3_mvf_obs = True     ; missing value flag for observational sasd default
    spectra4_mvf = False        ; missing value flag for iod
    spectra4_mvf_obs = True     ; missing value flag for observational iod default
    if (any(ismissing(atl3))) then
      print("Missing data detected for "+names(ee)+", power spectra function does not allow missing data, not creating Atlantic Nino spectra")
      spectra1_mvf = True
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        spectra1_mvf_obs = True    ; missing value flag for obs atl3
      end if
    else
      sdof1 = specx_anal(atl3,iopt,jave,pct)   ; pc1 already standardized
      splt1 = specx_ci(sdof1,val1,val2)
      if (OUTPUT_DATA.eq."True") then
        splt1!0 = "ncurves"
        splt1&ncurves = ispan(0,3,1)
        splt1&ncurves@long_name = "power spectra curves"
        splt1&ncurves@units = "1"
        splt1!1 = "frequency"
        splt1&frequency = sdof1@frq
        splt1&frequency@units = "1"
        splt1@units_info = "df refers to frequency interval; data are standardized so there are no physical units"
        splt1@units = "1/df"
        splt1@info = "(0,:)=spectrum,(1,:)=Markov red noise spectrum, (2,:)="+val1+"% confidence bound for Markhov, (3,:)="+val2+"% confidence bound for Markhov"
        z->atl3_spectra = set_varAtts(splt1,"Tropical Atlantic Equatorial Mode (monthly) power spectra, Markov spectrum and confidence curves","","") 
      end if
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        sdof1_obs = sdof1
        spectra1_mvf_obs = False    ; missing value flag for obs atl3
      end if
    end if
    if any(ismissing(amm)) then
      print("Missing data detected for "+names(ee)+", power spectra function does not allow missing data, not creating Atl AMM spectra")
      spectra2_mvf = True
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        spectra2_mvf_obs = True 
      end if
    else
      sdof2 = specx_anal(amm,iopt,jave,pct)   ; pc1 already standardized
      splt2 = specx_ci(sdof2,val1,val2)
      if (OUTPUT_DATA.eq."True") then
        splt2!0 = "ncurves"
        splt2&ncurves = ispan(0,3,1)
        splt2&ncurves@long_name = "power spectra curves"
        splt2&ncurves@units = "1"
        splt2!1 = "frequency"
        splt2&frequency = sdof2@frq
        splt2&frequency@units = "1"
        splt2@units_info = "df refers to frequency interval; data are standardized so there are no physical units"
        splt2@units = "1/df"
        splt2@info = "(0,:)=spectrum,(1,:)=Markov red noise spectrum, (2,:)="+val1+"% confidence bound for Markhov, (3,:)="+val2+"% confidence bound for Markhov"
        z->amm_spectra = set_varAtts(splt2,"Atlantic Dipole Mode (monthly) power spectra, Markov spectrum and confidence curves","","") 
      end if
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        sdof2_obs = sdof2
        spectra2_mvf_obs = False 
      end if
    end if
    if any(ismissing(sasd)) then
      print("Missing data detected for "+names(ee)+", power spectra function does not allow missing data, not creating Atl SASD spectra")
      spectra3_mvf = True
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        spectra3_mvf_obs = True    ; missing value flag for obs nino3.4
      end if
    else
      sdof3 = specx_anal(sasd,iopt,jave,pct)   ; pc1 already standardized
      splt3 = specx_ci(sdof3,val1,val2)
      if (OUTPUT_DATA.eq."True") then
        splt3!0 = "ncurves"
        splt3&ncurves = ispan(0,3,1)
        splt3&ncurves@long_name = "power spectra curves"
        splt3&ncurves@units = "1"
        splt3!1 = "frequency"
        splt3&frequency = sdof3@frq
        splt3&frequency@units = "1"
        splt3@units_info = "df refers to frequency interval; data are standardized so there are no physical units"
        splt3@units = "1/df"
        splt3@info = "(0,:)=spectrum,(1,:)=Markov red noise spectrum, (2,:)="+val1+"% confidence bound for Markhov, (3,:)="+val2+"% confidence bound for Markhov"
        z->sasd_spectra = set_varAtts(splt3,"Atlantic Dipole Mode (monthly) power spectra, Markov spectrum and confidence curves","","") 
      end if
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        sdof3_obs = sdof3
        spectra3_mvf_obs = True    ; missing value flag for obs nino3.4
      end if
    end if
    if any(ismissing(iod)) then
      print("Missing data detected for "+names(ee)+", power spectra function does not allow missing data, not creating IOD spectra")
      spectra4_mvf = True
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        spectra4_mvf_obs = True    ; missing value flag for obs nino3.4
      end if
    else
      sdof4 = specx_anal(iod,iopt,jave,pct)   ; pc1 already standardized
      splt4 = specx_ci(sdof4,val1,val2)
      if (OUTPUT_DATA.eq."True") then
        splt4!0 = "ncurves"
        splt4&ncurves = ispan(0,3,1)
        splt4&ncurves@long_name = "power spectra curves"
        splt4&ncurves@units = "1"
        splt4!1 = "frequency"
        splt4&frequency = sdof4@frq
        splt4&frequency@units = "1"
        splt4@units_info = "df refers to frequency interval; data are standardized so there are no physical units"
        splt4@units = "1/df"
        splt4@info = "(0,:)=spectrum,(1,:)=Markov red noise spectrum, (2,:)="+val1+"% confidence bound for Markhov, (3,:)="+val2+"% confidence bound for Markhov"
        z->iod_spectra = set_varAtts(splt4,"Indian Ocean Dipole (monthly) power spectra, Markov spectrum and confidence curves","","") 
        delete([/modname,fn,z/])
      end if
      if (isfilepresent2("obs_ts").and.ee.eq.0) then
        sdof4_obs = sdof4
        spectra4_mvf_obs = True    
      end if
    end if
    delete([/iopt,jave,pct/])

;---------PR Regressions coding-------------------------------------------------
    if (any(ismissing((/syear(ee),syear_pr(ee),eyear(ee),eyear_pr(ee)/)))) then
      prreg_plot_flag = 1
    else
      if (syear(ee).eq.syear_pr(ee)) then     ; check that the start and end years match for ts, tas, and psl
        if (eyear(ee).eq.eyear_pr(ee)) then
          prreg_plot_flag = 0
        else
          prreg_plot_flag = 1
        end if
      else
        prreg_plot_flag = 1
      end if
    end if 

    if (prreg_plot_flag.eq.0) then 
      pr = data_read_in(paths_pr(ee),"PRECT",syear_pr(ee),eyear_pr(ee))
      if (isatt(pr,"is_all_missing")) then
        prreg_plot_flag = 1
        delete(pr)
      end if
      
      if (prreg_plot_flag.eq.0) then     ; only continue if both PSL/TS fields are present
        if (OPT_CLIMO.eq."Full") then
          pr = rmMonAnnCycTLL(pr)
        else
          if CLIMO_SYEAR.lt.0.and.CLIMO_EYEAR.le.0 then
            ;if negative, the climatology is set relative to the run end.
            climo_syear=eyear(ee)+CLIMO_SYEAR
            climo_eyear=eyear(ee)+CLIMO_EYEAR
          else
            climo_syear=CLIMO_SYEAR
            climo_eyear=CLIMO_EYEAR
          end if                   
          check_custom_climo(names_pr(ee),syear_pr(ee),eyear_pr(ee),climo_syear,climo_eyear)
          temp_arr = pr
          delete(temp_arr&time)
          temp_arr&time = cd_calendar(pr&time,1)
          climo = clmMonTLL(temp_arr({climo_syear*100+1:climo_eyear*100+12},:,:))                 
          delete(temp_arr)
          pr   = calcMonAnomTLL(pr,climo) 
          delete(climo)
        end if
        ;add back in detrending - not included in psl.nao - perhaps needs commenting out again
        pr = (/ dtrend_msg_n(ispan(0,dimsizes(pr&time)-1,1),pr,False,False,0) /)  
      end if
    end if

    if (prreg_plot_flag.eq.0) then
      atl3_pr = pr(0,:,:)
      atl3_pr = (/ regCoef(atl3,pr(lat|:,lon|:,time|:)) /)
      amm_pr = pr(0,:,:)
      amm_pr = (/ regCoef(amm,pr(lat|:,lon|:,time|:)) /)
      sasd_pr = pr(0,:,:)
      sasd_pr = (/ regCoef(sasd,pr(lat|:,lon|:,time|:)) /)
      iod_pr = pr(0,:,:)
      iod_pr = (/ regCoef(iod,pr(lat|:,lon|:,time|:)) /)
      delete(pr)
    end if

    if (OUTPUT_DATA.eq."True").and.(prreg_plot_flag.eq.0) then
      modname = str_sub_str(names_pr(ee)," ","_")
      bc = (/"/","'","(",")"/)
      do gg = 0,dimsizes(bc)-1
        modname = str_sub_str(modname,bc(gg),"_")
      end do
      fn = getenv("OUTDIR")+modname+".cvdp_data.cpl.sst.pr."+syear_pr(ee)+"-"+eyear_pr(ee)+".nc"
      if (.not.isfilepresent2(fn)) then
        z = addfile(fn,"c")
        z@source = "NCAR Climate Analysis Section's Climate Variability Diagnostics Package v"+getenv("VERSION")
        z@notes = "Data from "+names_pr(ee)+" from "+syear_pr(ee)+"-"+eyear_pr(ee)
        if (OPT_CLIMO.eq."Full") then
          z@climatology = syear_pr(ee)+"-"+eyear_pr(ee)+" climatology removed prior to all calculations (other than means)"
        else
          z@climatology = climo_syear+"-"+climo_eyear+" climatology removed prior to all calculations (other than means)"
        end if
        z@Conventions = "CF-1.6"
      else
        z = addfile(fn,"w")
      end if
      z->atl3_pr_regression = set_varAtts(atl3_pr,"precipitation regressed onto the Tropical Atlantic Equatorial Mode (Monthly)","","")    
      z->amm_pr_regression = set_varAtts(amm_pr,"precipitation regressed onto the Tropical Atlantic Meridional Mode (Monthly)","","")    
      z->sasd_pr_regression = set_varAtts(sasd_pr,"precipitation regressed onto the South Atlantic Subtropical Dipole Mode (Monthly)","","")    
      z->iod_pr_regression = set_varAtts(iod_pr,"precipitation regressed onto the Indian Ocean Dipole Index (Monthly)","","")    
      delete(z)
      delete([/modname,fn/])
    end if 

;-------------------------------------------------------------------
    if (isvar("z")) then
      delete(z)
    end if
;========================================================================
    res = True
    res@mpProjection = "WinkelTripel"
    res@mpGeophysicalLineColor = "gray42"
    res@mpPerimOn    = False
    res@mpGridLatSpacingF =  90            ; change latitude  line spacing
    res@mpGridLonSpacingF = 180.           ; change longitude line spacing
    res@mpGridLineColor   = "transparent"  ; trick ncl into drawing perimeter
    res@mpGridAndLimbOn   = True           ; turn on lat/lon lines  
    res@mpFillOn = False
    res@mpCenterLonF = -20.
    res@mpOutlineOn = True  
    res@gsnDraw      = False
    res@gsnFrame     = False
    res@vpYF = 0.95
    res@vpHeightF = 0.3
    res@vpXF = 0.2
    res@vpWidthF = 0.6
    
;     res@cnFillMode = "RasterFill"
    res@cnLevelSelectionMode = "ExplicitLevels"
    
    if (COLORMAP.eq."0") then
      res@cnLevels = fspan(-.65,.65,27)
    end if
    if (COLORMAP.eq."1") then
      res@cnLevels = fspan(-.8,.8,17)
    end if

    res@cnLineLabelsOn = False
    res@cnFillOn        = True
    res@cnLinesOn       = False
    res@lbLabelBarOn    = False

    res@gsnLeftStringOrthogonalPosF = -0.05
    res@gsnLeftStringParallelPosF = .005
    res@gsnRightStringOrthogonalPosF = -0.05
    res@gsnRightStringParallelPosF = 0.96
    res@gsnRightString = ""
    res@gsnLeftString = ""
    res@gsnLeftStringFontHeightF = 0.014
    res@gsnCenterStringFontHeightF = 0.018
    res@gsnRightStringFontHeightF = 0.014
    res@gsnLeftString = syear(ee)+"-"+eyear(ee)
    res@gsnRightString = ""

    res4 = res    ; res4 = pr regression resources     
    if (COLORMAP.eq.0) then
      res4@cnLevels := fspan(-.7,.7,15)  
    else
      res4@cnLevels := fspan(-.5,.5,11)     
    end if
    
    res@gsnCenterString = names(ee)
    map1(ee) = gsn_csm_contour_map(wks_atl3,atl3_patt,res)
    delete([/atl3_patt/])
    map2(ee) = gsn_csm_contour_map(wks_amm,amm_patt,res)
    delete([/amm_patt/])
    res@gsnRightString = sasd@pcvar
    map3(ee) = gsn_csm_contour_map(wks_sasd,sasd_patt,res)
    delete([/sasd_patt,res@gsnRightString/])
    map4(ee) = gsn_csm_contour_map(wks_iod,iod_patt,res)
    delete([/iod_patt/])
    
    if (prreg_plot_flag.eq.0) then     ; PR regressions
      res4@gsnCenterString = names_pr(ee)
      reg_atl3_pr(ee) = gsn_csm_contour_map(wks_atl3_pr,atl3_pr,res4)
      reg_amm_pr(ee) = gsn_csm_contour_map(wks_amm_pr,amm_pr,res4)
      reg_sasd_pr(ee) = gsn_csm_contour_map(wks_sasd_pr,sasd_pr,res4)
      reg_iod_pr(ee) = gsn_csm_contour_map(wks_iod_pr,iod_pr,res4)
      delete([/atl3_pr,amm_pr,sasd_pr,iod_pr/])
    end if

    pres = True
    pres@vpXF = 0.07
    pres@trYMinF = 0.
    pres@trXMinF = 0.0
    if (isfilepresent2("obs_ts").and.ee.ge.1.and.spectra_mvf_obs.eq.False) then   
      pres@trYMaxF = 1.1*max((/max(splt1(0,:)),max(sdof1_obs@spcx),max(splt2(0,:)),max(sdof2_obs@spcx),\
                     max(splt3(0,:)),max(sdof3_obs@spcx),max(splt4(0,:)),max(sdof4_obs@spcx)/))
    else
      if all(.not.(/spectra1_mvf,spectra2_mvf,spectra3_mvf,spectra4_mvf/)) then   
        pres@trYMaxF = 1.1*max((/max(splt1(0,:)),max(splt2(0,:)),max(splt3(0,:)),max(splt4(0,:))/))
      else
        pres@trYMaxF = 80.
      end if  
    end if
    pres@trXMaxF = 0.0832
    pres@tiYAxisString = "Power"              ; yaxis
    pres@xyLineColor   =  "black"         
    pres@gsnFrame      = False
    pres@gsnDraw       = False
    
    pres@tmXBLabelDeltaF = -.8
    pres@tmXTLabelDeltaF = -.8
    pres@pmLegendDisplayMode    = "Never"
    pres@xyLineThicknesses   = (/3.5,2.,1.,1./) 
    pres@xyDashPatterns      = (/0,0,0,0/)
    pres@xyLineColors        = (/"foreground","red","blue","green"/)      
    pres@xyLabelMode = "custom"
    pres@xyLineLabelFontColors = pres@xyLineColors 
    pres@xyExplicitLabels = (/"","",val1*100+"%",val2*100+"%"/)
    pres@tmXTOn = True
    pres@tmYROn = False
    pres@tmXTLabelsOn = True
    pres@tmXUseBottom = False
    pres@tmXTMode   = "Explicit"  
    pres@tmXBMode   = "Explicit"            
    pres@tmXTValues = (/".00167",".00833",".01667",".02778",".0416",".0556",".0832"/)
    pres@tmXTLabels = (/"50","10","5","3","2","1.5","1"/)           
    pres@tmXBValues = (/".0",".01",".02",".03",".042",".056",".083"/)
    pres@tmXBLabels =     pres@tmXBValues
    pres@tmXTLabelFontHeightF = 0.018
    pres@tmXBLabelFontHeightF = 0.018
    pres@tmYLLabelFontHeightF = 0.018
    pres@tiYAxisString = "Variance" ;"Power (~S~o~N~C~S~2~N~ / cycles mo~S~-1~N~)"              ; yaxis
    pres@tiXAxisString = "Frequency (cycles mo~S~-1~N~)"
    pres@tiMainString = ""
    pres@txFontHeightF = 0.015
    pres@xyLineLabelFontHeightF = 0.022
    pres@tiXAxisFontHeightF = 0.025
    pres@tiYAxisFontHeightF = 0.025
    pres@tiMainFontHeightF = 0.03
    pres@gsnRightStringOrthogonalPosF = -0.115
    
    pres@tiMainOn = False
    pres@gsnCenterString = "Period (years)"
    pres@gsnCenterStringFontHeightF =    pres@tiYAxisFontHeightF
    pres@gsnRightStringFontHeightF  =    pres@tiYAxisFontHeightF - 0.005
    pres@gsnRightString = syear(ee)+"-"+eyear(ee)+"  "
    pres@gsnLeftString = "" 
    if (wks_type.eq."png") then
      pres@xyLineThicknessF   = 3.5
      res@mpGeophysicalLineThicknessF = 2.  
    else
      pres@xyLineThicknessF   = 1.5
      res@mpGeophysicalLineThicknessF = 1.  
    end if
    pres@gsnCenterString = names(ee)
    if (spectra1_mvf.eq.False) then
      pspec1(ee) = gsn_csm_xy(wks_atl3_spec,sdof1@frq,splt1,pres) 
      if (isfilepresent2("obs_ts").and.ee.ge.1.and.spectra_mvf_obs.eq.False) then
        pres@xyLineColors        = (/"gray70","black","black","black"/)
        ;pres@xyCurveDrawOrder = "PreDraw"
        pres@gsnCenterString = ""
        pres@gsnRightString = ""
        pspec_obs1(ee) = gsn_csm_xy(wks_atl3_spec,sdof1_obs@frq,sdof1_obs@spcx,pres)
        overlay(pspec1(ee),pspec_obs1(ee)) 
        delete(pres@xyCurveDrawOrder)
      end if     
      delete([/sdof1,splt1/])
    end if
    ;amm spectra
    if (spectra2_mvf.eq.False) then
      pspec2(ee) = gsn_csm_xy(wks_amm_spec,sdof2@frq,splt2,pres) 
      if (isfilepresent2("obs_ts").and.ee.ge.1.and.spectra_mvf_obs.eq.False) then
        pres@xyLineColors        = (/"gray70","black","black","black"/)
        ;pres@xyCurveDrawOrder = "PreDraw"
        pres@gsnCenterString = ""
        pres@gsnRightString = ""
        pspec_obs2(ee) = gsn_csm_xy(wks_amm_spec,sdof2_obs@frq,sdof2_obs@spcx,pres)
        overlay(pspec2(ee),pspec_obs2(ee)) 
        delete(pres@xyCurveDrawOrder)
      end if     
      delete([/sdof2,splt2/])
    end if
    ;sasd spectra
    if (spectra3_mvf.eq.False) then
      pspec3(ee) = gsn_csm_xy(wks_sasd_spec,sdof3@frq,splt3,pres) 
      if (isfilepresent2("obs_ts").and.ee.ge.1.and.spectra_mvf_obs.eq.False) then
        pres@xyLineColors        = (/"gray70","black","black","black"/)
        ;pres@xyCurveDrawOrder = "PreDraw"
        pres@gsnCenterString = ""
        pres@gsnRightString = ""
        pspec_obs3(ee) = gsn_csm_xy(wks_sasd_spec,sdof3_obs@frq,sdof3_obs@spcx,pres)
        overlay(pspec3(ee),pspec_obs3(ee)) 
        delete(pres@xyCurveDrawOrder)
      end if     
      delete([/sdof3,splt3/])
    end if
    ;iod spectra
    if (spectra4_mvf.eq.False) then
      pspec4(ee) = gsn_csm_xy(wks_iod_spec,sdof4@frq,splt4,pres) 
      if (isfilepresent2("obs_ts").and.ee.ge.1.and.spectra_mvf_obs.eq.False) then
        pres@xyLineColors        = (/"gray70","black","black","black"/)
        ;pres@xyCurveDrawOrder = "PreDraw"
        pres@gsnCenterString = ""
        pres@gsnRightString = ""
        pspec_obs4(ee) = gsn_csm_xy(wks_iod_spec,sdof4_obs@frq,sdof4_obs@spcx,pres)
        overlay(pspec4(ee),pspec_obs4(ee)) 
        delete(pres@xyCurveDrawOrder)
      end if     
      delete([/sdof4,splt4/])
    end if 
    
    xyres = True
    xyres@gsnDraw = False
    xyres@gsnFrame = False
    xyres@gsnRightString = ""
    xyres@gsnLeftString = ""
    xyres@gsnFrame = False
    xyres@gsnYRefLine = 0.0
    xyres@gsnYRefLineColor = "gray42"
    xyres@gsnXYBarChart = False
    xyres@gsnAboveYRefLineColor = 185
    xyres@gsnBelowYRefLineColor = 35
    xyres@xyLineThicknessF = 0.1
    xyres@xyLineColor = "gray70"
    xyres@tiYAxisString = ""
    if (nsim.le.5) then
      xyres@tmXBLabelFontHeightF = 0.0125
      xyres@tmYLLabelFontHeightF = 0.0125
      xyres@gsnStringFontHeightF = 0.017     
    else
      xyres@tmXBLabelFontHeightF = 0.018
      xyres@tmYLLabelFontHeightF = 0.018
      xyres@gsnStringFontHeightF = 0.024
    end if
    xyres@vpXF = 0.05
    xyres@vpHeightF = 0.15
    if (SCALE_TIMESERIES.eq."True") then
      xyres@vpWidthF = 0.9*((nyr(ee)*1.)/nyr_max)
    else
      xyres@vpWidthF = 0.9
    end if
    xyres@gsnCenterString = ""
    
    xyres@trXMinF = syear(ee)-.5
    xyres@trXMaxF = eyear(ee)+1.5
    
    xyres@gsnCenterString = names(ee)
    xyplot1(ee) = gsn_csm_xy(wks_ts,fspan(syear(ee),eyear(ee)+.91667,dimsizes(atl3)),atl3,xyres)
    xyplot2(ee) = gsn_csm_xy(wks_ts,fspan(syear(ee),eyear(ee)+.91667,dimsizes(amm)),amm,xyres)
    xyplot3(ee) = gsn_csm_xy(wks_ts,fspan(syear(ee),eyear(ee)+.91667,dimsizes(sasd)),sasd,xyres)
    xyplot4(ee) = gsn_csm_xy(wks_ts,fspan(syear(ee),eyear(ee)+.91667,dimsizes(iod)),iod,xyres)
    delete([/val1,val2,atl3,amm,sasd,iod/])
  end do
  
  panres = True
  panres@gsnMaximize = True
  panres@gsnPaperOrientation = "portrait"
  panres@gsnPanelLabelBar = True
  panres@gsnPanelYWhiteSpacePercent = 3.0
  panres@pmLabelBarHeightF = 0.05
  panres@pmLabelBarWidthF = 0.55
  panres@lbTitleOn = False
  panres@lbBoxLineColor = "gray70"
  if (nsim.le.4) then
    if (nsim.eq.1) then
      panres@txFontHeightF = 0.022
      panres@gsnPanelBottom = 0.50
    else
      panres@txFontHeightF = 0.0145
      panres@gsnPanelBottom = 0.50
    end if
  else
    panres@txFontHeightF = 0.016
    panres@gsnPanelBottom = 0.05
  end if
  panres@txString = "Tropical Atlantic Equatorial Mode (Monthly)"
  ncol = floattointeger(sqrt(nsim))
  nrow = (nsim/ncol)+mod(nsim,ncol)  
  gsn_panel2(wks_atl3,map1,(/nrow,ncol/),panres)
  delete(wks_atl3)
  panres@txString = "Tropical Atlantic Meridional Mode (Monthly)"
  gsn_panel2(wks_amm,map2,(/nrow,ncol/),panres)
  delete(wks_amm)
  panres@txString = "South Atlantic Subtropical Dipole Mode (Monthly)"
  gsn_panel2(wks_sasd,map3,(/nrow,ncol/),panres)
  delete(wks_sasd)
  panres@txString = "Indian Ocean Dipole (Monthly)"
  gsn_panel2(wks_iod,map4,(/nrow,ncol/),panres)
  delete(wks_iod)

  delete(panres@gsnPanelLabelBar)
  if (SCALE_TIMESERIES.eq."True") then
    tt = ind(nyr.eq.nyr_max)
    panres@gsnPanelScalePlotIndex = tt(0)
    delete(tt)
  end if
  if (nsim.le.12) then
    lp = (/nsim,1/)
  else
    lp = (/nrow,ncol/)  ;(/nsim/2+1,nsim/8+1/)  
  end if

  panres@txString = "Tropical Atlantic Equatorial Mode (Monthly)"
  gsn_panel2(wks_ts,xyplot1,lp,panres)  
  panres@txString = "Tropical Atlantic Meridional Mode (Monthly)"
  gsn_panel2(wks_ts,xyplot2,lp,panres)  
  panres@txString = "South Atlantic Subtropical Dipole Mode (Monthly)"
  gsn_panel2(wks_ts,xyplot3,lp,panres)  
  panres@txString = "Indian Ocean Dipole (Monthly)"
  gsn_panel2(wks_ts,xyplot4,lp,panres)  
  delete(wks_ts)

  panres@txString = "Tropical Atlantic Equatorial Mode (Monthly)"
  gsn_panel2(wks_atl3_spec,pspec1,(/nrow,ncol/),panres)  
  delete(wks_atl3_spec)
  panres@txString = "Tropical Atlantic Meridional Mode (Monthly)"
  gsn_panel2(wks_amm_spec,pspec2,(/nrow,ncol/),panres)  
  delete(wks_amm_spec)
  panres@txString = "South Atlantic Subtropical Dipole Mode (Monthly)"
  gsn_panel2(wks_sasd_spec,pspec3,(/nrow,ncol/),panres)  
  delete(wks_sasd_spec)
  panres@txString = "Indian Ocean Dipole (Monthly)"
  gsn_panel2(wks_iod_spec,pspec4,(/nrow,ncol/),panres)  
  delete(wks_iod_spec)
  
  panres@gsnPanelLabelBar = True
  panres@txString = "ATL3 PR Regressions (Monthly)"
  gsn_panel2(wks_atl3_pr,reg_atl3_pr,(/nrow,ncol/),panres)
  delete(wks_atl3_pr)  
  panres@txString = "AMM PR Regressions (Monthly)"
  gsn_panel2(wks_amm_pr,reg_amm_pr,(/nrow,ncol/),panres)
  delete(wks_amm_pr)  
  panres@txString = "SASD PR Regressions (Monthly)"
  gsn_panel2(wks_sasd_pr,reg_sasd_pr,(/nrow,ncol/),panres)
  delete(wks_sasd_pr)  
  panres@txString = "IOD PR Regressions (Monthly)"
  gsn_panel2(wks_iod_pr,reg_iod_pr,(/nrow,ncol/),panres)
  delete(wks_iod_pr)  

  delete([/map1,map2,map3,map4,pspec1,pspec2,pspec3,pspec4,syear,eyear,nyr,nyr_max,lp,\
         xyplot1,xyplot2,xyplot3,xyplot4,reg_atl3_pr,reg_amm_pr,reg_sasd_pr,reg_iod_pr/])

  if (wks_type.eq."png") then  
     system("mv "+OUTDIR+"cpl.sst.timeseries.000001.png "+OUTDIR+"atl3.timeseries.png")
     system("mv "+OUTDIR+"cpl.sst.timeseries.000002.png "+OUTDIR+"amm.timeseries.png")
     system("mv "+OUTDIR+"cpl.sst.timeseries.000003.png "+OUTDIR+"sasd.timeseries.png")
     system("mv "+OUTDIR+"cpl.sst.timeseries.000004.png "+OUTDIR+"iod.timeseries.png")
  else
     system("psplit "+OUTDIR+"tio.timeseries.ps "+OUTDIR+"cpl.sst_ind")
     system("mv "+OUTDIR+"cpl.sst_ind0001.ps "+OUTDIR+"atl3.timeseries.ps")
     system("mv "+OUTDIR+"cpl.sst_ind0002.ps "+OUTDIR+"amm.timeseries.ps")
     system("mv "+OUTDIR+"cpl.sst_ind0003.ps "+OUTDIR+"sasd.timeseries.ps")
     system("mv "+OUTDIR+"cpl.sst_ind0004.ps "+OUTDIR+"iod.timeseries.ps")
     system("rm "+OUTDIR+"cpl.sst.timeseries.ps")
  end if


  print((/"Finished: cpl.sst.ncl"/))
end

