;This file creates images to let one see the proportion of summer rainfall variability associated with El Nino
;It uses plotCVDPcomparisonMaps in cvdp_data.functions.ncl to do this.
;Rather than feeding in a filename (for the plot to export to), this gives an example of inputting a workstation directly,
; along with manual control over the panellling section. This allows the script to have two different variables side by side.

;First load the ncl scripts
load "~/ncl/cvdp-synda/ncl_scripts/cvdp_data.functions.ncl"

data_dir="data" ;Point the directory containing the output from the CVDP

opt=True ;Set some options for the individual panels
opt@cnFillPalette="MPL_YlOrRd"
opt@cnLevelSelectionMode = "ManualLevels"   
opt@cnMinLevelValF = 0.
opt@cnMaxLevelValF = 1.5
opt@cnLevelSpacingF = 0.1
opt@lbLabelBarOn = True
opt@tiMainString = (/"C20 Reanalysis","Ens. Mn in piControl"/)

opt_pan=True ;Set some options for the panelling itself
opt_pan@dims=(/1,2/);put plots side-by-side
opt_pan@gsnFrame=False
opt_pan@lbLabelBarOn = False
opt_pan@lbTitleString = "" ;set empty label title
opt_pan@gsnMaximize=True

;Use the routines in cvdp_data.functions.ncl to create the plot
plotCVDPcomparisonMaps(data_dir,(/"C20_Reanalysis","piControl"/),"tas_spatialstddev_dec","plot_10stddev_midHolocene.1",opt,opt_pan)

;Different color bar for changes in decadal variabilty
opt@cnFillPalette="BlueDarkRed18"
opt@RATIO=True
opt@CONSISTENCY=True 
opt@cnMinLevelValF = 0.75
opt@cnMaxLevelValF = 1.25
opt@cnLevelSpacingF = 0.05
delete(opt@tiMainString)
opt@tiMainString = "midHolocene Ens. Mn. as ratio of piControl Ens. Mn." 
;Use the routines in cvdp_data.functions.ncl to create the plot
plotDiffEnsMnMaps(data_dir,"midHolocene","piControl","tas_spatialstddev_dec","plot_10stddev_midHolocene.2",opt,opt_pan)
