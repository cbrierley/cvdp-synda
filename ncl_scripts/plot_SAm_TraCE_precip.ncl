;This file creates images to let one see the proportion of summer rainfall variability associated with El Nino
;It uses plotCVDPcomparisonMaps in cvdp_data.functions.ncl to do this.
;Rather than feeding in a filename (for the plot to export to), this gives an example of inputting a workstation directly,
; along with manual control over the panellling section. This allows the script to have two different variables side by side.

;First load the ncl scripts
load "~/ncl/cvdp-synda/ncl_scripts/cvdp_data.functions.ncl"

data_dir="/home/ucfaccb/ncl/cvdp-synda/TraCE_by_kyr.output" ;Point the directory containing the output from the CVDP

kas=(/"1-0ka","12-11ka","14-13ka","21-20ka"/)

wks=gsn_open_wks("pdf","plot_SAm_TraCE_precip")
  wks@VERBOSE="True"
opt=True
  opt@cnFillPalette="CBR_wet"
  opt@mpProjection="CylindricalEqualArea"
  opt@mpLimitMode="LatLon"
  opt@mpMinLonF=-110
  opt@mpMaxLonF=-30
  opt@mpMinLatF=-30
  opt@mpMaxLatF=30
  opt@gsnLeftString=(/"1-0ka","1-0ka","12-11ka","12-11ka","14-13ka","14-13ka","21-20ka","21-20ka"/)
  opt@gsnRightString=(/"DJF","JJA","DJF","JJA","DJF","JJA","DJF","JJA"/)
  opt@gsnStringFontHeightF=0.03
  opt@tiMainString=""
  opt@gsnCenterString=""
  opt@lbLabelBarOn=False
  opt@cnLevels=(/0.5,1.,2.,3.,4.,5.,6.,7.,8.,9.,10./)
opt_pan=True
  opt_pan@lbTitleOn=False
  opt_pan@lbOrientation = "vertical"
  opt_pan@gsnPanelLabelBar=True
  opt_pan@lbLabelFontHeightF=0.016

plotCVDPcomparisonMaps(data_dir,kas,(/"pr_spatialmean_djf","pr_spatialmean_jja"/),wks,opt,opt_pan)
delete(wks)

