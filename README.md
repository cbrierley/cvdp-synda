This folder contains the additions and modifications made to the CVDP routines by Chris Brierley. The original programs are documented elsewhere. I believe that the following provides a relatively complete list of the changes.

## Scientific additions

* **Atlantic Coupled Modes**
  three new coupled modes have been added and are calculated by `ncl_scripts/cpl.stt.ncl`. They will form the basis of a manuscript that I'm working on with Ilana Wainer in Sao Paolo. The (monthly) timeseries, sst pattern, pr regression and power specrtra are computed for each mode. Each coupled mode has additional documentation in webpage.ncl
    * **AMM** The Atlantic Meridional Mode is the primary mode of climate variability in the tropical Atlantic. It's associated with variations in the north-south temperature contrast between the two hemispheres in the tropics.
    * **ATL3** This is the second mode of the tropical Atlantic , also called the Atlantic Nino. We've used an area-averaged SST index for it.
    * **SASD** The South Atlantic Subtropical Dipole is an EOF-based climate mode that influences rainfall over South America and Southern Africa.   
  
* **Decadal standard deviations**
  to help with investigations into natural variability in forecasts, I've added computation of the decadal standard deviations.

* **Spectra of global average timeseries**
  the power spectra are now plotted from the monthly global mean timeseries 

* **IOD spatial patterns**
  I have promoted the Indian Ocean Dipole from just being an "additional timeseries". It now has an SST spatial pattern, a monthly precipitiaton regression pattern and a power spectra. I haven't gone the extra step and looked at seasonal or composites

* **Precipitation Indices** 
  After a similar fashion to the sst indices, I have added a series of precipitation based indices. These are calculated by the program `ncl_scripts/pr.indices.ncl`. They only measure precip over land (not ocean) in an attempt to fit better with observed records. The added indices are
    * England & Wales precipitation
    * Sahel rainfall
    * N.E. Brazil rainfall
    * All-India rainfall
    * East Asian monsoon
    * East African rainfall
    * Baikal precip

* **Climatological Annual Cycle**
  Some timeseries now have the climatological annual cycle added to them as an attribute (@AnnCycle). I've mainly done this for non-EOF-based timeseries, where it will have some physical meaning (primiarly SST indices).

## Operational modifications
* **namelist location**
  The active location of the namelist has been changed to be in the output folder. This edit was to permit me to have a single CVDP directory. If there is no namelist already in the output folder, the version from the current directory is copied there. This invovled edits to many of the ncl_scripts
  
* **relative climatologies**
  v4.1 allows the option for a single 'custom' climatological period to be selected across the simulations. This is great, but not helpful for idealised simulations where the model years do not align. Therefore, I have added the capability to select relative climatologies. These are set in driver.ncl as negative numbers, and are taken relative to the end of each simulation (not the start). This choice was motivated by experiments like CMIP's abrupt4x and 1pctCO2, where you would want to focus on the changed climate at the end of the run.
  
* **timing of the mean plots**
  The computation scripts have been edited such that now if a 'custom' climatology is specified, the means are only calculated over that climatological period - rather than the full simulation as at present. I've gone with this approach, so that I can extract data from the mean state for use in subsequent analysis more easily.   
  
* **webpage**
  The actual formatting of the webpage has only undergone minor edits to encompass the new calculations. I have changed the way it is written by ncl to be a table rather than an ascii file. This allows for new plots to be more easily appended to the html and helps future proof the script better.
  
## Plotting of output data
I have created a series of functions to process and plot the netcdf output of the CVDP sftware. These are stored in `ncl_scripts/cvdp_data.functions.ncl`.

* **Ensemble mean maps** The data contained within a lat-lon variable can be plotted directly using the procedure `plotCVDPComparisonMaps`. This will either select and plot a single file (based on the filename) or a create and plot an ensemble average of files (that match a filename search string). All files are interpolated on a 1-degree grid to enable the averaging. Plot and panel resources can be passed to the routine, to give the user greater control over the results.

* **Ensemble difference maps** The difference between lat-lon variables can be plotted directly using the procedure `plotDiffEnsMnMaps`. This can be used to plot the difference between single simulations, a single fields vs an ensemble mean (useful for either ensemble-mean biases or your run against exiting ones), or the mean differences between two ensembles (say rcp2.6 vs rcp8.5). The relevant data files are identified by matching to two text search strings (and the appropriate behaviour determined) single file (based on the filename) or a create and plot an ensemble average of files (that match a filename search string). Plot and panel resources can be passed to the routine, to give the user greater control over the results.

* **Timeseries plots of an ensemble** The routines are not so advanced for timeseries plots (because they never involve changing grids). The routine `plotEnsTimeseries` will identify and plot all instances of a timeseries variable that are contained within filenames that match a search string. It can be passed plot and panel resources, as well as some timeseries modifiers:
    * `ts_opt@renorm_climo` to alter the climatological reference period 
    * `ts_opt@subset` to just select a subset of the timeseries
    * `ts_opt@SEASON` to plot a single 3-month season
    * `ts_opt@DETREND` to remove a linear trend from the timeseries
    * `ts_opt@RUN_STDDEV` to plot only the running standard deviation of the timeseries rather than the timeseries itself

* **Table of available simulation years** Whilst the plotting routines will automatically create ensemble mean, it isn't always obvious what model have made up that ensemble. This routine creates a table to show how many come from each simulation in the ensemble with the specified variable. The output formats are either csv or tex.

* **Example use scripts**
    * `plotDiffEnsMnMaps_example.ncl` shows a pretty minimal instance, that plots the ensemble mean difference in the NPO.
    * `plotCVDPComparisonMaps_example.ncl` creates a slightly more sophisticated figure plots maps of a single "simulation" compared to an ensemble mean
    * `example_cvdp_data.functions_BrierleyWainer.ncl` gives a full bells & whistles example that creates sufficient plots to form the the basis of a publication

* ** Full list of `cvdp_data.functions.ncl routines**
    * `find_files_wVar` identifies data filenames matching a string that contain the requested variable
    * `read_latlon_var` opens a file and extracts the specified variable - with an option to interpolate onto a common 1degree grid
    * `read_diff_latlon_var` opens two files (hopefully of the same model) and extracts the change in a specified variable - with an option to interpolate onto a common 1degree grid
    * `extract_latlon_areastat` opens a file, extracts an area subset from a variable and then calculates either the weighted mean or stddev in that area
    * `find_pair_files_wVar` identifies matching pairs of filenames differing only by the final portion of the name (scenario) that both contain the requested variable
    * `read_ts` opens a file and loads a specified timeseries variable. It can also apply some transformations on that time as specified with `ts_opt` above
    * `stat_ts_var` opens a timeseries variable (using `read_ts`) and returns a single number containing either the mean, standard deviation, skew, kurtosis or percentage variance contained within a principal component.
    * `read_ts_all` uses `read_ts` to create an array holding an ensemble of timeseries
    * `roughlatlonplot` forks a quick plot of a lat/lon field to the screen, with an optional contour range taken from `field@valid_range` and an optional map extent from `field@plotLatLons`. No plot resources or workstation needs to be set 
    * `plotCVDPcomparisonMaps` *see above*
    * `plotDiffEnsMnMaps` *see above*
    * `plotDiffEnsTimeseries` *see above*
    * `createTableGCMsExptsYears` procedure to create a table. It needs to be provided with names of the experiments and the names of the models - as these form the column and row titles respectively.
  
  
  
  