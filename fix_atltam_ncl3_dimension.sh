#!/bin/bash
DATA_DIR=data

cd $DATA_DIR
cvdp_files=*.cvdp_data.[0-9]*-*.nc
for fil in $cvdp_files
do
  echo Working on $fil
  ncks -O -v atltam_timeseries_mon -x $fil tmp.$fil
  ncks -O -v atltam_timeseries_mon $fil tmp.nc
  ncrename -d  ncl3,time tmp.nc
  ncks -A -v atltam_timeseries_mon tmp.nc tmp.$fil
  mv tmp.$fil $fil
done
rm tmp.nc
