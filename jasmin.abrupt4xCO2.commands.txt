# Make the $OUTDIR/namelist...
NAME="abrupt4xCO2"
OUTDIR=output.$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $NAME.driver.ncl
echo "CCSM4 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/NCAR/CCSM4/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,} | 1850 | 1999" > $OUTDIR/namelist 
echo "bcc-csm1-1 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/BCC/bcc-csm1-1/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 160 | 309" >> $OUTDIR/namelist
echo "CNRM-CM5 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/CNRM-CERFACS/CNRM-CM5/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1850 | 1999" >> $OUTDIR/namelist
echo "CSIRO-Mk3-6-0 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1 | 150" >> $OUTDIR/namelist
echo "EC-EARTH-2-2 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/ICHEC/EC-EARTH/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1850 | 1999" >> $OUTDIR/namelist
echo "FGOALS-g2 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/LASG-CESS/FGOALS-g2/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 490 | 639" >> $OUTDIR/namelist
echo "FGOALS-s2 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/LASG-IAP/FGOALS-s2/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1850 | 1999" >> $OUTDIR/namelist
echo "GISS-E2-R 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/NASA-GISS/GISS-E2-R/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1850 | 1999" >> $OUTDIR/namelist
echo "HadGEM2-ES 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/MOHC/HadGEM2-ES/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1860 | 2009" >> $OUTDIR/namelist
echo "IPSL-CM5A-LR 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/IPSL/IPSL-CM5A-LR/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1850 | 1999" >> $OUTDIR/namelist
#echo "MIROC 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/MIROC/MIROC-ESM/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1 | 150" >> $OUTDIR/namelist
echo "MPI-ESM-LR 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/MPI-M/MPI-ESM-LR/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1850 | 1999" >> $OUTDIR/namelist
#echo "MRI-CGCM3 0 abrupt4xCO2 | /badc/cmip5/data/cmip5/output1/MRI/MRI-CGCM3/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}  | 1851 | 2000" >> $OUTDIR/namelist

#alter the climatology to be the final 40 years of the simulation
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $NAME.driver.ncl
sed -i 's:climo_syear = 1971:climo_syear = -39:g' $NAME.driver.ncl
sed -i 's:climo_eyear = 2000:climo_eyear = -0:g' $NAME.driver.ncl
sed -i 's:machine_casesen = "True":machine_casesen = "CMIP_ONLY":g' $NAME.driver.ncl
 
ncl -n $NAME.driver.ncl >& $NAME.driver.log &
