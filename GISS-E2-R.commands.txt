# Find the data for piControl
synda search GISS-E2-R piControl Amon variable=ts,tas,psl,pr
synda search GISS-E2-R piControl OImon sic
sudo synda install -y GISS-E2-R piControl Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R piControl OImon sic


# Find the data for midHolocene
synda search GISS-E2-R midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
synda search GISS-E2-R midHolocene r1i1p1 OImon sic
sudo synda install -y GISS-E2-R midHolocene r1i1p1 OImon sic

# Find the data for lgm
synda search GISS-E2-R lgm  Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R lgm Amon variable=ts,tas,psl,pr
synda search GISS-E2-R lgm  OImon sic
sudo synda install -y GISS-E2-R lgm OImon sic

# Find the data for historical
synda search GISS-E2-R historical r1i1p1,r1i1p2 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R historical r1i1p1,r1i1p2 Amon variable=ts,tas,psl,pr
synda search GISS-E2-R historical r1i1p1,r1i1p2 OImon sic
sudo synda install -y GISS-E2-R historical r1i1p1,r1i1p2 OImon sic

# Find the data for 1pctCO2
synda search GISS-E2-R 1pctCO2 r1i1p1,r1i1p2,r1i1p3 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R 1pctCO2 r1i1p1,r1i1p2,r1i1p3 Amon variable=ts,tas,psl,pr
synda search GISS-E2-R 1pctCO2 r1i1p1,r1i1p2,r1i1p3 OImon sic
sudo synda install -y GISS-E2-R 1pctCO2 r1i1p1,r1i1p2,r1i1p3 OImon sic

# Find the data for past1000
synda search GISS-E2-R past1000 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R past1000 Amon variable=ts,tas,psl,pr
synda search GISS-E2-R past1000 OImon sic
sudo synda install -y GISS-E2-R past1000 OImon sic

# Make the $OUTDIR/namelist...
NAME="GISS-E2-R"
OUTDIR=$NAME.output
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $NAME.driver.ncl
echo "GISS-E2-R 0 piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20121017/ | 3981 | 4530" >> $OUTDIR/namelist 
echo "GISS-E2-R 1 piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p2/v20121017/ | 3616| 4120" >> $OUTDIR/namelist 
#echo "GISS-E2-R 2 piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p3/v20121017/ | 3586 | 4090" >> $OUTDIR/namelist 
echo "GISS-E2-R 141 piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p141/v20121017/ | 0850 | 2012" >> $OUTDIR/namelist 
echo "GISS-E2-R 142 piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p142/v20120517/ | 2790 | 2889" >> $OUTDIR/namelist 
echo "GISS-E2-R 0 midHolocene | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/midHolocene/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120716/ | 2500 | 2599" >> $OUTDIR/namelist
echo "GISS-E2-R 0 lgm | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/lgm/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p150/v20120516/ | 3000 | 3099" >> $OUTDIR/namelist
echo "GISS-E2-R 151 lgm | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p151/v20120516/ | 3000 | 3099" >> $OUTDIR/namelist
echo "GISS-E2-R 0 historical | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/historical/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20121015/ | 1850 | 2005" >> $OUTDIR/namelist
echo "GISS-E2-R 2 historical | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/historical/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p2/v20121015/ | 1850 | 2005" >> $OUTDIR/namelist
echo "GISS-E2-R 0 1pctCO2 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120803/ | 1850 | 1989" >> $OUTDIR/namelist
echo "GISS-E2-R 1 1pctCO2 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p2/v20120803/ | 1850 | 1989" >> $OUTDIR/namelist
echo "GISS-E2-R 2 1pctCO2 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p3/v20120803/ | 1850 | 1989" >> $OUTDIR/namelist

echo "GISS-E2-R 121 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p121/v20120531/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 122 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p122/v20120605/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 123 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p123/v20120907/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 124 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p124/v20120516/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 125 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p125/v20120516/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 126 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p126/v20120913/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 127 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p127/v20120824/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 128 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p128/v20121213/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R 1221 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1221/v20130522/ | 850 | 1850" >> $OUTDIR/namelist

ncl -n $NAME.driver.ncl >& $NAME.driver.log &
