# Find the data for piControl
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE piControl r1i1p1 Amon variable=ts,tas,psl,pr
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE piControl r1i1p1 OImon sic
sudo synda install -y cmip5.output1.CSIRO-QCCCE.CSIRO-Mk3-6-0.piControl.mon.atmos.Amon.r1i1p1.v20120607 variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE piControl r1i1p1 OImon sic

# Find the data for midHolocene
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 OImon sic
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 Omon msftmyz
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 OImon sic

# Find the data for historical
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 Amon variable=ts,tas,psl,pr
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 OImon sic
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 Omon msftmyz
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 OImon sic

# Find the data for 1pctCO2
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 OImon sic
synda search CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 Omon msftmyz
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 OImon sic


# Make the $OUTDIR/namelist...
NAME="CSIRO-Mk3-6-0"
OUTDIR=$NAME.output
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $NAME.driver.ncl
echo "CSIRO-Mk3-6-0 0 piControl | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v1,v20120607}/ | 1 | 500" > $OUTDIR/namelist 
echo "CSIRO-Mk3-6-0 0 midHolocene | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/ | 1 | 100" >> $OUTDIR/namelist
echo "CSIRO-Mk3-6-0 0 historical | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/ | 1850 | 2005" >> $OUTDIR/namelist
echo "CSIRO-Mk3-6-0 0 1pctCO2 | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/ | 1 | 140" >> $OUTDIR/namelist

ncl -n $NAME.driver.ncl >& $NAME.driver.log &
