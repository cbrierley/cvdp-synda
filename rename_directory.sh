#!/bin/bash

linknames="palaeovar palaeoVar Palaeovar PalaeoVar paleovar Paleovar PaleoVar paleovardata Paleovardata PaleoVardata PaleoVarData pmip_paleovar pmip_PaleoVar PMIP_paleovar PMIP_PaleoVar  PMIPVar pmipvardata pmip_vardata PMIPvardata PMIPvarData PMIPVardata PMIP_Vardata PMIP_VarData vardata varData Vardata VarData"

cd ~/public_html
for nm in $linknames
do
  rm $nm
  ln -s PMIPVarData $nm
done
